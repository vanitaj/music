<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
//======================= Admin Breadcrumbs ==================//
// home
Breadcrumbs::for('a-home', function ($trail) {
$trail->push('Home', route('admin.dashboard'));
});

//blog
Breadcrumbs::for('a-blog', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Blog', route('blogs.index'));
});

//school
Breadcrumbs::for('a-school', function ($trail) {
    $trail->parent('a-home');
    $trail->push('School', route('schools.index'));
});

//teacher
Breadcrumbs::for('a-teacher', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Teacher', route('teachers.index'));
});

//grade
Breadcrumbs::for('a-grade', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Grade', route('grade.index'));
});

//term
Breadcrumbs::for('a-term', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Term', route('term.index'));
});

//lesson
Breadcrumbs::for('a-lesson', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Lesson', route('lesson.index'));
});

//submission
Breadcrumbs::for('a-submission', function ($trail) {
    $trail->parent('a-lesson');
    $trail->push('Submission', route('lesson.index'));
});

//assessment
Breadcrumbs::for('a-assessment', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Assessment', route('assessment.index'));
});

//contact-us
Breadcrumbs::for('a-contact-us', function ($trail) {
    $trail->parent('a-home');
    $trail->push('Contact Us', route('contact-us.index'));
});
//======================= Admin Breadcrumbs Ends ==================//