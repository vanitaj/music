<?php

use App\Http\Controllers\Admin\AssessmentController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\AdminLoginController;
use App\Http\Controllers\Admin\Auth\AdminController;
use App\Http\Controllers\Admin\Auth\PasswordController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\GradeController;
use App\Http\Controllers\Admin\LessonController;
use App\Http\Controllers\Admin\SchoolController;
use App\Http\Controllers\Admin\TeacherController;
use App\Http\Controllers\Admin\TermController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix'=> 'admin','middleware' => ['checkstatus']],function() {

    // // Admin Auth Routes
    Route::get('/login', [AdminLoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('/login', [AdminLoginController::class, 'login'])->name('admin.login.submit');
    Route::get('logout/', [AdminLoginController::class, 'logout'])->name('admin.logout');
    Route::get('forget-password', [PasswordController::class, 'showForgetPasswordForm'])->name('admin.forget.password.get');
    Route::post('forget-password', [PasswordController::class, 'sendPasswordResetToken'])->name('admin.forget.password.post'); 
    Route::get('reset-password/{token}', [PasswordController::class, 'showPasswordResetForm'])->name('admin.reset.password.get');
    Route::post('reset-password', [PasswordController::class, 'resetPassword'])->name('admin.reset.password.post');
    //Admin Auth Routes
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    //Blogs
    Route::get('blogs/check-slug',[BlogController::class,'checkSlug'])->name('blog.check_slug');
    Route::resource ( 'blogs', BlogController::class );

    //School
    Route::resource ( 'schools', SchoolController::class );

    //Teacher
    Route::resource ( 'teachers', TeacherController::class );

    //Grade
    Route::get('grade/teacher-list',[GradeController::class,'teacherList'])->name('grade.teacher-list');
    Route::resource ( 'grade', GradeController::class );

    //Term
    Route::get('term/grade-list',[TermController::class,'gradeList'])->name('term.grade-list');
    Route::get('term/position-list',[TermController::class,'positionList'])->name('term.position-list');
    Route::resource ( 'term', TermController::class );

    //lesson
    Route::get('lesson/grade-list',[LessonController::class,'gradeList'])->name('lesson.grade-list');
    Route::get('lesson/submission/{id}',[LessonController::class,'submissionList'])->name('lesson.submission-list');
    Route::get('lesson/submission-download/{id}',[LessonController::class,'submissionDownload'])->name('lesson.submission-download');
    Route::get('lesson/position-list',[LessonController::class,'positionList'])->name('lesson.position-list');
    Route::get('lesson/term-list',[LessonController::class,'termList'])->name('lesson.term-list');
    Route::post('lesson/store-resource',[LessonController::class,'storeResource'])->name('lesson.store-resource');
    Route::post('lesson/store-assignment',[LessonController::class,'storeAssignment'])->name('lesson.store-assignment');
    Route::resource ( 'lesson', LessonController::class );

    //assessment
    Route::get('assessment/grade-list',[AssessmentController::class,'gradeList'])->name('assessment.grade-list');
    Route::get('assessment/term-list',[AssessmentController::class,'termList'])->name('assessment.term-list');
    Route::resource ( 'assessment', AssessmentController::class );

    //contact us
    Route::resource ( 'contact-us', ContactUsController::class );
});