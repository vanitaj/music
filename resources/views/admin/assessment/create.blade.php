@extends('admin.layouts.app')

 @section('content_header')
Assessments
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-assessment') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('assessment.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                       <!-- select -->
                       <div class="form-group">
                    <label for="school">School</label>
                    <select class="form-control" id="school_id" name="school_id">
                      <option value="">--select--</option>
                      @foreach($schoolLists as $school)
                        <option value="{{$school->id}}" @if(old('school_id') == $school->id) selected @endif>{{$school->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label  for="school">Grade</label>
                    <select class="form-control"   id="grade_id" name="grade_id">
                     <option value="">--select--</option>
                    </select>
                    <input id="gid" type="hidden" value="{{old('grade_id')}}"/>
                  </div>
                  <div class="form-group">
                    <label  for="school">Term</label>
                    <select class="form-control"   id="term_id" name="term_id">
                     <option value="">--select--</option>
                    </select>
                    @error('term_id')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                    <input id="tid" type="hidden" value="{{old('term_id')}}"/>
                  </div>
                  <div class="form-group">
                    <label for="url">Url</label>
                    <input type="text" name="url" class="form-control" id="url" placeholder="Enter url" value="{{old('url')}}">
                    @error('url')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="editor1" class="form-control ckeditor" >{{ old('description') }}</textarea>
                    @error('description')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('assessment.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
$(function () {
    var schoolId = "";
    var gId = "";
    var tId = "";
    if($('#school_id').val()){
      schoolId = $('#school_id').val();
    }
    if($('#gid').val()){
      gId = $('#gid').val();
    }

    if($('#tid').val()){
      tId = $('#tid').val();
    }
    $.get("{{ route('assessment.grade-list') }}", 
      { 'school_id': schoolId }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        if(len>0){
            console.log(data['gradeList']);
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var oldGradeId = "";
                if(gId == id)
                {
                  oldGradeId = "selected";
                }
                var option = "<option value='"+id+"' "+oldGradeId+">"+name+"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
    console.log('term id',tId);
    //term List
    $.get("{{ route('assessment.term-list') }}", 
      { 'grade_id': gId }, 
      function( data ) {
        $("#term_id option").remove();
        var len = data['termList'].length;
        if(len>0){
            console.log(data['termList']);
            for(var i=0; i<len; i++){
                var id = data['termList'][i].id;
                var name = data['termList'][i].term_name;
                var oldTermId = "";
                if(tId == id)
                {
                  oldTermId = "selected";
                }
                var option = "<option value='"+id+"' "+oldTermId+">"+name+"</option>";
                $("#term_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#term_id").append(option); 
        }
      }
    );

  });
  
  $('#school_id').change(function(e) 
  {
      $.get("{{ route('assessment.grade-list') }}", 
      { 'school_id': $(this).val() }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        
        if(len>0){
          var option = "<option value=''>--select--</option>";
          $("#grade_id").append(option); 
            // console.log(data['gradeList']);
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
  });

  //term list

  $('#grade_id').change(function(e) 
  {
      $.get("{{ route('assessment.term-list') }}", 
      { 'grade_id': $(this).val() }, 
      function( data ) {
        $("#term_id option").remove();
        var len = data['termList'].length;
        if(len>0){
            console.log(data['termList']);
            for(var i=0; i<len; i++){
                var id = data['termList'][i].id;
                var name = data['termList'][i].term_name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#term_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#term_id").append(option); 
        }
      }
    );
  });
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
      term_id: {
        required: true,
      },
      url: {
        required: true,
      },
      description: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
      term_id: {
        required: "Please select term",
      },
      url: {
        required: "Please enter url",
      },
      description: {
        required: "Please enter description",
      },
   
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection