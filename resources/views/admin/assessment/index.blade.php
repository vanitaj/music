@extends('admin.layouts.app')
@section('content_header')
Assessments
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-assessment') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('assessment.create')}}">Add New</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>School Name</th>
                    <th>Grade Name</th>
                    <th>Term Name</th>
                    <th>Assessment URL</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($assessmentLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$row['term']['grade']['school']['name']}}</td>
                        <td>{{$row['term']['grade']['grade_name']}}</td>
                        <td>{{$row['term']['term_name']}}</td>
                        <td> 
                            <a class="btn btn-primary btn-sm" href="{{$row['url']}}" target="__blank">
                              <i class="fas fa-eye">
                              </i>                              
                            </a>
                        </td>
                        <td>
                          <a class="btn btn-primary btn-sm" href="{{ route('assessment.edit',$row->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>                              
                          </a>
                          <a class="btn btn-danger btn-sm delete-record delete-row" href="{{ route('assessment.destroy',$row->id) }}" data-id="{{$row->id}}">
                              <i class="fas fa-trash">
                              </i>
                          </a>
                          <form id="delete-form-{{$row->id}}" method="post" action="{{ route('assessment.destroy',$row->id) }}" display="none">
                            @csrf
                            @method('DELETE')
                          </form>
                        </td>
                      </tr>    
                     @endforeach   
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection