@extends('admin.layouts.app')

 @section('content_header')
Assessments
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-assessment') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('assessment.update',$assessment->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                       <!-- select -->
                   <div class="form-group">
                        <label for="school">School</label>
                        <select class="form-control" id="school_id" name="school_id">
                          <option value="{{$assessment['term']['grade']['school']['id']}}">{{$assessment['term']['grade']['school']['name']}}</option>
                        </select>
                  </div>
                  <div class="form-group">
                  <label>Grade</label>
                  <select class="form-control" id="grade_id" name="grade_id">
                    <option value="{{$assessment['term']['grade']['id']}}">{{$assessment['term']['grade']['grade_name']}}</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Term</label>
                  <select class="form-control" id="term_id" name="term_id">
                    <option value="{{$assessment['term_id']}}">{{$assessment['term']['term_name']}}</option>
                  </select>
                </div>
                <div class="form-group">
                    <label for="url">Url</label>
                      <input type="text" name="url" class="form-control" id="url" placeholder="Enter ulr" value="{{print_value(old('url'),$assessment->url)}}">
                      @error('url')
                        <span style="color:red">{{ $message }}</span>
                      @enderror
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="editor1" class="form-control ckeditor" >{{print_value(old('description'),$assessment->description)}}</textarea>
                    @error('description')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('assessment.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
      term_id: {
        required: true,
      },
      url: {
        required: true,
      },
      description: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
      term_id: {
        required: "Please select term",
      },
      url: {
        required: "Please enter url",
      },
      description: {
        required: "Please enter description",
      },
   
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection