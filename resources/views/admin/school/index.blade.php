@extends('admin.layouts.app')
@section('content_header')
Schools
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-school') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('schools.create')}}">Add New</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>School Name</th>
                    <th>School Address</th>
                    <th>School Contact No</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($schoolLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->address}}</td>
                        <td>{{$row->official_contact_no}}</td>
                        <td>{{ $row->status ? 'Active' : 'Not Active' }}</td>
                        <td>
                          <a class="btn btn-primary btn-sm" href="{{ route('schools.edit',$row->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>                              
                          </a>
                          {{--<!-- <a class="btn btn-danger btn-sm delete-record delete-row" href="{{ route('schools.destroy',$row->id) }}" data-id="{{$row->id}}">
                              <i class="fas fa-trash">
                              </i>
                          </a>
                          <form id="delete-form-{{$row->id}}" method="post" action="{{ route('schools.destroy',$row->id) }}" display="none">
                            @csrf
                            @method('DELETE')
                          </form> -->--}}
                        </td>
                      </tr>    
                     @endforeach   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection