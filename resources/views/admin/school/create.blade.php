@extends('admin.layouts.app')

 @section('content_header')
Schools
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-school') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('schools.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">School Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter school name" value="{{old('name')}}">
                    @error('name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="">School Address</label>
                    <textarea name="address" id="address" class="form-control" placeholder="Enter school address">{{ old('address') }}</textarea>
                    @error('address')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="official_contact_no">School Official Contact No</label>
                    <input type="text" name="official_contact_no" class="form-control" id="official_contact_no" placeholder="Enter school official contact no" value="{{old('official_contact_no')}}">
                    @error('official_contact_no')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                      <input type="checkbox" class="custom-control-input" id="customSwitch3" name="status" @if (old('status') == 1) checked @endif value="1">
                      <label class="custom-control-label" for="customSwitch3">Status</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('schools.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>

$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      name: {
        required: true,
      },
      address:{
        required: true,
      },
      official_contact_no:{
        required: true,
      }
    },
    messages: {
      name: {
        required: "Please enter a school name",
      },
      address: {
        required: "Please enter a address",
      },
      official_contact_no: {
        required: "Please enter a school official contact no",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection