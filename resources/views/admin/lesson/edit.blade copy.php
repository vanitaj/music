@extends('admin.layouts.app')

 @section('content_header')
Lessons
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-lesson') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('lesson.update',$lesson->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <!-- select -->
                  <div class="form-group">
                    <label for="school">School</label>
                      <select class="form-control" id="school_id" name="school_id">
                        <option value="{{$lesson['term']['grade']['school_id']}}">{{$lesson['term']['grade']['school']['name']}}</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label>Grade</label>
                    <select class="form-control" id="grade_id" name="grade_id">
                      <option value="{{$lesson['term']['grade']['id']}}">{{$lesson['term']['grade']['grade_name']}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Term</label>
                    <select class="form-control" id="term_id" name="term_id">
                      <option value="{{$lesson['term']['id']}}">{{$lesson['term']['term_name']}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="title">Title</label>
                      <input type="text" name="title" class="form-control" id="title" placeholder="Enter title " value="{{print_value(old('title'),$lesson->title)}}">
                      @error('title')
                        <span style="color:red">{{ $message }}</span>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="term_name">Lesson Name</label>
                    <input type="text" name="lesson_name" class="form-control" id="lesson_name" placeholder="Enter lesson name" value="{{print_value(old('lesson_name'),$lesson->lesson_name)}}">
                    @error('lesson_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="how_do_they_learn">How do they Learn</label>
                    <textarea name="how_do_they_learn" id="editor1" class="form-control ckeditor" >{{print_value(old('how_do_they_learn'),$lesson->how_do_they_learn)}}</textarea>
                    @error('how_do_they_learn')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="what_are_the_learing_outcome">What are the learning outcome</label>
                    <textarea name="what_are_the_learing_outcome" id="editor2" class="form-control ckeditor" >{{print_value(old('what_are_the_learing_outcome'),$lesson->what_are_the_learing_outcome)}}</textarea>
                    @error('what_are_the_learing_outcome')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <hr />
                  <div class="form-group text-center">
                    <label for="resouce"><u>Resources</u></label>
                  </div>
                  <div id="newRow">
                    @foreach($lesson['resource'] as $key=>$resource)
                      <div id="inputFormRow">
                        <input type="hidden" name="resource_id[]" value="{{$resource->id}}">
                        <div class="form-group">
                          @if($resource->document_type == 'YouTube Link')
                            <a href="{{$resource->url}}"  target="__blank"><i class='fas fa-film' style='font-size:48px;'></i></a>
                          @else
                          <hr>
                            <a href="{{asset($resource->url)}}" target="__blank">  <img src="{{resource_file($resource->url)}}" alt="image" style="width:8%;"></a>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="description">Description</label>
                          <textarea name="description[]" id="description" class="form-control" required>{{print_value(old('description'),$resource->description) }}</textarea>
                        </div>
                        <div class="form-group">
                          <button id="removeRow" type="button" class="btn btn-danger addRow" ><i class="fas fa-minus"></i></button>
                        </div>
                      </div>
                    @endforeach
                  </div>
                  <button id="addRow" type="button" class="btn btn-primary"><i class='fas fa-plus'></i></button>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('lesson.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
  // add row
  var rowNum = 0;
  
  $("#addRow").click(function () {
      rowNum++;
  
      var html = '';
      html += '<div id="inputFormRow">';
      html+= '<div class="form-group">';
      html+= '<label  for="type">Document Type</label>';
      html+= '<select class="form-control"   id="type-'+rowNum+'" data-id="'+rowNum+'" name="type[]" required>';
      html+= '<option value="Upload Document">Upload Document</option>';
      html+= '<option value="YouTube Link">YouTube Link</option>';
      html+= '</select>';
      html+= '</div>';
      html+= '<div class="form-group" id="upload-'+rowNum+'"data-id="'+rowNum+'">';
      html+= '<label for="exampleInputFile">Upload Document</label>';
      html+= '<div class="input-group">';
      html+= '<input type="file" class="form-control" id="upload_document_'+rowNum+'" name="url[]" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,audio/mpeg,audio/"  />';
      html+= '</div>';
      html+= '</div>';
      html+= '<div class="form-group" id="url-'+rowNum+'" data-id="'+rowNum+'"style="display: none;">';
      html+= '<label for="term_name">YouTube Link</label>';
      html+= '<input type="text" name="url[]" class="form-control" id="youtube_link_"'+rowNum+'" placeholder="Enter youtube link" >';
      html+= '</div>';
      html+= '<div class="form-group">';
      html+= '<label for="description">Description</label>';
      html+= '<textarea name="description[]" id="editor2" class="form-control ckeditor" required></textarea>';     
      html+= '</div>';
      html+= '<div class="form-group">';
      html += '<button id="removeRow" type="button" class="btn btn-danger addRow" ><i class="fas fa-minus"></i></button>';
      html+= '</div>';
      html+= '</div>';
      $('#newRow').append(html);
   });

// remove row
$(document).on('click', '#removeRow', function () {
    $(this).closest('#inputFormRow').remove();
});
$(document).ready(function(){
        //$('img').on("error", handleError);
        $('#quickForm').on('change', 'select', function (e) {
            var id = $(this).attr('data-id');
            console.log('selected option id',id)
            $( "select option:selected").each(function(){
             
                if($(this).attr("value")=="Upload Document"){
                    $("#upload-"+id).show();   
                    $("#url-"+id).hide();      
                }
                if($(this).attr("value")=="YouTube Link"){
                    $("#upload-"+id).hide();
                    $("#url-"+id).show();                      
                }
            });
        }).change();
    });
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
      term_id: {
        required: true,
      },
      title: {
        required: true,
      },
      lesson_name: {
        required: true,
      },
      how_do_they_learn: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
      what_are_the_learing_outcome: {
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
      term_id: {
        required: "Please select term",
      },
      title: {
        required: "Please enter title",
      },
      lesson_name: {
        required: "Please enter lesson name",
      },
      how_do_they_learn:{
        required: "Please enter how do they learn",
      },
      what_are_the_learing_outcome:{
        required: "Please enter what are the learing outcome",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection