@extends('admin.layouts.app')
@section('content_header')
Lessons
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-lesson') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('lesson.create')}}">Add New</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>School Name</th>
                    <th>Grade Name</th>
                    <th>Term Name</th>
                    <th>Lesson Name</th>
                    <th>Completed</th>
                    <th>Feedback</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($lessonLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$row['term']['grade']['school']['name']}}</td>
                        <td>{{$row['term']['grade']['grade_name']}}</td>
                        <td>{{$row['term']['term_name']}}</td>
                        <td>{{$row['lesson_name']}}</td>
                        <td>{{$row['is_complete'] ? 'Yes' : 'No' }}</td>
                        <td>
                          @if($row['feedback'] )
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg-{{$row->id}}">
                                View
                            </button>
                            <div class="modal fade" id="modal-lg-{{$row->id}}">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Feedback</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="control-label col-sm-4" for="name">Feedback:</label>
                                                <div class="col-sm-8">{{  $row->feedback }}</div>
                                            </div>
                                         
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                          @else
                           - 
                           @endif</td>
                        <td>
                          <a class="btn btn-primary btn-sm" href="{{ route('lesson.edit',$row->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>                              
                          </a>
                          <a class="btn btn-danger btn-sm delete-record delete-row" href="{{ route('lesson.destroy',$row->id) }}" data-id="{{$row->id}}">
                              <i class="fas fa-trash">
                              </i>
                          </a>
                          <form id="delete-form-{{$row->id}}" method="post" action="{{ route('lesson.destroy',$row->id) }}" display="none">
                            @csrf
                            @method('DELETE')
                          </form>
                        </td>
                      </tr>    
                     @endforeach   
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection