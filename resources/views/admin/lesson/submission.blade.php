@extends('admin.layouts.app')
@section('content_header')
Submissions
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-submission') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-header">
                    <div class="row">
                      <div class="col-7">
                        <h5>{{$lesson ->lesson_name}}</h5>
                      </div>
                      <div class="col-5">
                        <a class="btn btn-default mx-2" href="{{route('lesson.edit',$lesson->id)}}" >Back</a>
                        <a class="btn btn-default" href="{{route('lesson.index')}}" >Lessons</a>
                   
                      </div>
                    </div>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>File Name</th>
                    <th>Submitted By(Teacher)</th>
                    <th>Submitted On</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($submissionLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{basename($row['url'])}}</td>
                        <td>@if(isset($row['teacher'])){{$row['teacher']['name']}} @else - @endif</td>
                        <td>{{formatted_date($row['created_at'])}}</td>
                        <td>
                            <a class="btn btn-primary btn-sm" href="{{asset($row['url'])}}" target="_blank">
                              <i class="fas fa-eye">
                              </i>                              
                            </a>
                            <a class="btn btn-success btn-sm" href="{{route('lesson.submission-download',$row->id)}}" target="_blank">
                              <i class="fas fa-download">
                              </i>                              
                            </a>
                        </td>
                      </tr>    
                     @endforeach   
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection