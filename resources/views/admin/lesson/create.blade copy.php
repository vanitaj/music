@extends('admin.layouts.app')

 @section('content_header')
Lessons
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-term') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('lesson.index')}}" >Back</a>
              </div>
             <!-- form start -->
              <form id="quickForm" action="{{route('lesson.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                   <!-- select -->
                  <div class="form-group">
                    <label for="school">School</label>
                    <select class="form-control" id="school_id" name="school_id">
                      <option value="">--select--</option>
                      @foreach($schoolLists as $school)
                        <option value="{{$school->id}}" @if(old('school_id') == $school->id) selected @endif>{{$school->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label  for="school">Grade</label>
                    <select class="form-control"   id="grade_id" name="grade_id">
                     <option value="">--select--</option>
                    </select>
                    <input id="gid" type="hidden" value="{{old('grade_id')}}"/>
                  </div>
                  <div class="form-group">
                    <label  for="school">Term</label>
                    <select class="form-control"   id="term_id" name="term_id">
                     <option value="">--select--</option>
                    </select>
                    <input id="tid" type="hidden" value="{{old('term_id')}}"/>
                  </div>
                  <div class="form-group">
                    <label for="term_name">What Do They Learn</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Enter what do they learn" value="{{old('title')}}">
                    @error('title')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="term_name">Lesson Name</label>
                    <input type="text" name="lesson_name" class="form-control" id="lesson_name" placeholder="Enter lesson name" value="{{old('lesson_name')}}">
                    @error('lesson_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="short_description">Short Description</label>
                    <textarea name="short_description" id="short_description" class="form-control" >{{ old('short_description') }}</textarea>
                    @error('short_description')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="how_do_they_learn">How do they Learn</label>
                    <textarea name="how_do_they_learn" id="editor1" class="form-control ckeditor" >{{ old('how_do_they_learn') }}</textarea>
                    @error('how_do_they_learn')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="what_are_the_learing_outcome">What are the learning outcome</label>
                    <textarea name="what_are_the_learing_outcome" id="editor2" class="form-control ckeditor" >{{ old('what_are_the_learing_outcome') }}</textarea>
                    @error('what_are_the_learing_outcome')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                 
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                      <font color="red">
                        <div id="position_list"></div>
                      </font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter lesson position" value="{{old('position')}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('lesson.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

    @if(session('lessonId'))
      <div class="container-fluid">
        <h2>Resources</h2>
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm1" action="{{route('lesson.store-resource')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="lessonId" value="{{session('lessonId')}}">
                <div class="card-body">
                  <div class="form-group text-center">
                    <label for="resouce"><u>Resources</u></label>
                  </div>
                  <div class="form-group resource-type">
                    <label  for="type">Document Type</label>
                    <select class="form-control" data-id="0"  id="type-0" name="resource[0][type]" >
                     <option value="Upload Document">Upload Document</option>
                     <option value="YouTube Link">YouTube Link</option>
                    </select>
                  </div>
                  <div class="form-group" id="upload-0">
                    <label for="exampleInputFile">Upload Document</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="upload-document-0" name="resource[0][url]" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,audio/mpeg,audio/"  required/>
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" id="url-0" style="display: none;">
                    <label for="term_name">YouTube Link</label>
                    <input type="text" name="resource[0][url]" class="form-control" id="youtube-link-0" placeholder="Enter youtube link" value="{{old('url')}}">
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="resource[0][description]" id="description-0" class="form-control" required>{{ old('description') }}</textarea>
                  </div>
                  <div id="newRow"></div>
                    <button id="addRow" type="button" class="btn btn-primary"><i class='fas fa-plus'></i></button>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary"  @if(session('lessonId') == null ) disabled @endif >Submit</button>
                  <a href="{{ route('lesson.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div>

      <div class="container-fluid">
        <h2>Assignment</h2>
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm2" action="{{route('lesson.store-assignment')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="lessonId" value="{{session('lessonId')}}">
                <div class="card-body">
                  <div class="form-group text-center">
                    <label for="resouce"><u>Assignment</u></label>
                  </div>
                  <div class="form-group">
                    <label  for="type">Document Type</label>
                    <select class="form-control" data-id="0"  id="assignment-type-0" name="assignment[0][type]" >
                     <option value="Upload Document">Upload Document</option>
                     <option value="YouTube Link">YouTube Link</option>
                    </select>
                  </div>
                  <div class="form-group" id="assignment-upload-0">
                    <label for="exampleInputFile">Upload Document</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="assignment-upload-document-0" name="assignment[0][url]" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,audio/mpeg,audio/"  required/>
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" id="assignment-url-0" style="display: none;">
                    <label for="term_name">YouTube Link</label>
                    <input type="text" name="assignment[0][url]" class="form-control" id="assignment-youtube-link-0" placeholder="Enter youtube link" value="{{old('url')}}">
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="assignment[0][description]" id="assignment-description-0" class="form-control" required>{{ old('description') }}</textarea>
                  </div>
                  <div id="newAssignmentRow"></div>
                    <button id="addAssignmentRow" type="button" class="btn btn-primary"><i class='fas fa-plus'></i></button>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary"  @if(session('lessonId') == null ) disabled @endif >Submit</button>
                  <a href="{{ route('lesson.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
      @endif
    </section>
@endsection

@section('script')
<script>

  // add row
  var rowNum = 0;
  $("#addRow").click(function () {
      rowNum++;
  
      var html = '';
      html += '<div id="inputFormRow">';
      html+= '<div class="form-group  resource-type">';
      html+= '<label  for="type">Document Type</label>';
      html+= '<select class="form-control"   id="type-'+rowNum+'" data-id="'+rowNum+'" name="resource['+rowNum+'][type]" required>';
      html+= '<option value="Upload Document">Upload Document</option>';
      html+= '<option value="YouTube Link">YouTube Link</option>';
      html+= '</select>';
      html+= '</div>';
      html+= '<div class="form-group" id="upload-'+rowNum+'"data-id="'+rowNum+'">';
      html+= '<label for="exampleInputFile">Upload Document</label>';
      html+= '<div class="input-group">';
      html+= '<div class="custom-file">';
      html+= '<input type="file" class="custom-file-input" id="upload-document-'+rowNum+'" name="resource['+rowNum+'][url]" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,audio/mpeg,audio/"  required/>';
      html+= '<label class="custom-file-label" for="exampleInputFile">Choose file</label>';
      html+= '</div>';
      html+= '</div>';
      html+= '</div>';
      html+= '<div class="form-group" id="url-'+rowNum+'" data-id="'+rowNum+'"style="display: none;">';
      html+= '<label for="term_name">YouTube Link</label>';
      html+= '<input type="text"  name="resource['+rowNum+'][url]"  class="form-control" id="youtube-link-'+rowNum+'" placeholder="Enter youtube link" >';
      html+= '</div>';
      html+= '<div class="form-group">';
      html+= '<label for="description">Description</label>';
      html+= '<textarea  name="resource['+rowNum+'][description]"  id="description-'+rowNum+'" class="form-control ckeditor" required></textarea>';     
      html+= '</div>';
      html+= '<div class="form-group">';
      html += '<button id="removeRow" type="button" class="btn btn-danger addRow" ><i class="fas fa-minus"></i></button>';
      html+= '</div>';
      html+= '</div>';
      $('#newRow').append(html);
  });

  // remove row
  $(document).on('click', '#removeRow', function () {
    $(this).closest('#inputFormRow').remove();
  });
  //file upload name
  $(document).on('change', '.custom-file-input', function (event) {
    //console.log(event.target.files[0].name);
    $(this).next('.custom-file-label').html(event.target.files[0].name);
  })
    $(document).ready(function(){
        $('#quickForm1').on('change', 'select', function (e) {
            var id = $(this).attr('data-id');
            $( ".resource-type select option:selected").each(function(){
                if($(this).attr("value")=="Upload Document"){
                    $("#upload-"+id).show();   
                    $("#url-"+id).hide();    
                    $("#upload-document-"+id).attr('required', 'required');  
                    $("#youtube-link-"+id).removeAttr('required'); 
                }
                if($(this).attr("value")=="YouTube Link"){
                    $("#upload-"+id).hide();
                    $("#url-"+id).show();      
                    $("#youtube-link-"+id).attr('required', 'required');  
                    $("#upload-document-"+id).removeAttr('required');                  
                }
            });
        });
    });

    
//assignment start

     // add Assignment row
  var rowAssignmentNum = 0;
  $("#addAssignmentRow").click(function () {
    rowAssignmentNum++;
  
      var html = '';
      html += '<div id="inputFormAssignmentRow">';
      html+= '<div class="form-group">';
      html+= '<label  for="type">Document Type</label>';
      html+= '<select class="form-control"   id="assignment-type-'+rowAssignmentNum+'" data-id="'+rowAssignmentNum+'" name="assignment['+rowAssignmentNum+'][type]" required>';
      html+= '<option value="Upload Document">Upload Document</option>';
      html+= '<option value="YouTube Link">YouTube Link</option>';
      html+= '</select>';
      html+= '</div>';
      html+= '<div class="form-group" id="assignment-upload-'+rowAssignmentNum+'"data-id="'+rowAssignmentNum+'">';
      html+= '<label for="exampleInputFile">Upload Document</label>';
      html+= '<div class="input-group">';
      html+= '<div class="custom-file">';
      html+= '<input type="file" class="custom-file-input" id="assignment-upload-document-'+rowAssignmentNum+'" name="assignment['+rowAssignmentNum+'][url]" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,audio/mpeg,audio/"  required/>';
      html+= '<label class="custom-file-label" for="exampleInputFile">Choose file</label>';
      html+= '</div>';
      html+= '</div>';
      html+= '</div>';
      html+= '<div class="form-group" id="assignment-url-'+rowAssignmentNum+'" data-id="'+rowAssignmentNum+'"style="display: none;">';
      html+= '<label for="term_name">YouTube Link</label>';
      html+= '<input type="text"  name="assignment['+rowAssignmentNum+'][url]"  class="form-control" id="assignment-youtube-link-'+rowAssignmentNum+'" placeholder="Enter youtube link" >';
      html+= '</div>';
      html+= '<div class="form-group">';
      html+= '<label for="description">Description</label>';
      html+= '<textarea  name="assignment['+rowAssignmentNum+'][description]"  id="assignment-description-'+rowAssignmentNum+'" class="form-control ckeditor" required></textarea>';     
      html+= '</div>';
      html+= '<div class="form-group">';
      html += '<button id="removeAssignmentRow" type="button" class="btn btn-danger addRow" ><i class="fas fa-minus"></i></button>';
      html+= '</div>';
      html+= '</div>';
      $('#newAssignmentRow').append(html);
   });

  // remove row
  $(document).on('click', '#removeAssignmentRow', function () {
    $(this).closest('#inputFormAssignmentRow').remove();
  });
    $(document).ready(function(){
        $('#quickForm2').on('change', 'select', function (e) {
            var id = $(this).attr('data-id');
            console.log('assignment selected option id',id)
            $( "select option:selected").each(function(){
             
                if($(this).attr("value")=="Upload Document"){
                  console.log('document-'+$(this).attr("value"));
                    $("#assignment-upload-"+id).show();   
                    $("#assignment-url-"+id).hide();    
                    $("assignment-upload-document-"+id).attr('required', 'required');  
                    $("#assignment-youtube-link-"+id).removeAttr('required'); 
                }
                if($(this).attr("value")=="YouTube Link"){
                  console.log('you tube-'+$(this).attr("value"));
                    $("#assignment-upload-"+id).hide();
                    $("#assignment-url-"+id).show();      
                    $("#assignment-youtube-link-"+id).attr('required', 'required');  
                    $("#assignment-upload-document-"+id).removeAttr('required');                  
                }
            });
        });
    });

    //assignment end
  $(function () {
    var schoolId = "";
    var gId = "";
    var tId = "";
    if($('#school_id').val()){
      schoolId = $('#school_id').val();
    }
    if($('#gid').val()){
      gId = $('#gid').val();
    }

    if($('#tid').val()){
      tId = $('#tid').val();
    }
    $.get("{{ route('lesson.grade-list') }}", 
      { 'school_id': schoolId }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        if(len>0){
            console.log(data['gradeList']);
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var oldGradeId = "";
                if(gId == id)
                {
                  oldGradeId = "selected";
                }
                var option = "<option value='"+id+"' "+oldGradeId+">"+name+"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
    console.log('term id',tId);
    //term List
    $.get("{{ route('lesson.term-list') }}", 
      { 'grade_id': gId }, 
      function( data ) {
        $("#term_id option").remove();
        var len = data['termList'].length;
        if(len>0){
          var option = "<option value=''>--select--</option>";
            console.log(data['termList']);
            for(var i=0; i<len; i++){
                var id = data['termList'][i].id;
                var name = data['termList'][i].term_name;
                var oldTermId = "";
                if(tId == id)
                {
                  oldTermId = "selected";
                }
                var option = "<option value='"+id+"' "+oldTermId+">"+name+"</option>";
                $("#term_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#term_id").append(option); 
        }
      }
    );

  });
  
  $('#school_id').change(function(e) 
  {
      $.get("{{ route('lesson.grade-list') }}", 
      { 'school_id': $(this).val() }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        
        if(len>0){
          var option = "<option value=''>--select--</option>";
          $("#grade_id").append(option); 
            // console.log(data['gradeList']);
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
  });

  //term list

  $('#grade_id').change(function(e) 
  {
      $.get("{{ route('lesson.term-list') }}", 
      { 'grade_id': $(this).val() }, 
      function( data ) {
        $("#term_id option").remove();
        var len = data['termList'].length;
        if(len>0){
          var option = "<option value=''>--select--</option>";
          $("#term_id").append(option); 
            console.log(data['termList']);
            for(var i=0; i<len; i++){
                var id = data['termList'][i].id;
                var name = data['termList'][i].term_name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#term_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#term_id").append(option); 
        }
      }
    );
  });
  jQuery('#term_id').change(function(e) {
      jQuery.get('{{ route('lesson.position-list') }}', 
      { 'term_id': $(this).val() }, 
      function( data ) {
        $('#position_list').empty();
        var positionLen =  data['position'].length;
        var positions =[];  
        console.log(data['position']);
            for(var j=0; j<positionLen;j++)
            {
              positions.push(data['position'][j].position);
            }
            var position = " Occupied Positon :-"+positions+"";
            $('#position_list').append(position);
        
      }
    );
  });
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
      term_id: {
        required: true,
      },
      title: {
        required: true,
      },
      position: {
        required: true,
      },
      short_description: {
        required: true,
      },
      lesson_name: {
        required: true,
      },
      'resource[0][description]': {
        required: true,
      },
     
      how_do_they_learn: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
      what_are_the_learing_outcome: {
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
      term_id: {
        required: "Please select term",
      },
      title: {
        required: "Please enter what do they learn",
      },
      lesson_name: {
        required: "Please enter lesson name",
      },
      position: {
        required: "Please enter lesson position",
      },
      short_description: {
        required: "Please enter lesson short description",
      },
      how_do_they_learn:{
        required: "Please enter how do they learn",
      },
      what_are_the_learing_outcome:{
        required: "Please enter what are the learing outcome",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
});
</script>
@endsection