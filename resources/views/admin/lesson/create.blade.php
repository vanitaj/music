@extends('admin.layouts.app')

 @section('content_header')
Lessons
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-term') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('lesson.index')}}" >Back</a>
              </div>
             <!-- form start -->
              <form id="quickForm" action="{{route('lesson.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                   <!-- select -->
                  <div class="form-group">
                    <label for="school">School</label>
                    <select class="form-control" id="school_id" name="school_id">
                      <option value="">--select--</option>
                      @foreach($schoolLists as $school)
                        <option value="{{$school->id}}" @if(old('school_id') == $school->id) selected @endif>{{$school->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label  for="school">Grade</label>
                    <select class="form-control"   id="grade_id" name="grade_id">
                     <option value="">--select--</option>
                    </select>
                    <input id="gid" type="hidden" value="{{old('grade_id')}}"/>
                  </div>
                  <div class="form-group">
                    <label  for="school">Term</label>
                    <select class="form-control"   id="term_id" name="term_id">
                     <option value="">--select--</option>
                    </select>
                    <input id="tid" type="hidden" value="{{old('term_id')}}"/>
                  </div>
                  <div class="form-group">
                    <label for="term_name">What Do They Learn</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Enter what do they learn" value="{{old('title')}}">
                    @error('title')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="term_name">Lesson Name</label>
                    <input type="text" name="lesson_name" class="form-control" id="lesson_name" placeholder="Enter lesson name" value="{{old('lesson_name')}}">
                    @error('lesson_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="short_description">Short Description</label>
                    <textarea name="short_description" id="short_description" class="form-control" >{{ old('short_description') }}</textarea>
                    @error('short_description')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="how_do_they_learn">How do they Learn</label>
                    <textarea name="how_do_they_learn" id="editor1" class="form-control ckeditor" >{{ old('how_do_they_learn') }}</textarea>
                    @error('how_do_they_learn')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="what_are_the_learing_outcome">What are the learning outcome</label>
                    <textarea name="what_are_the_learing_outcome" id="editor2" class="form-control ckeditor" >{{ old('what_are_the_learing_outcome') }}</textarea>
                    @error('what_are_the_learing_outcome')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="assignment">Assignment</label>
                    <textarea name="assignment" id="editor2" class="form-control ckeditor" >{{ old('assignment') }}</textarea>
                    @error('assignment')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                 
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                      <font color="red">
                        <div id="position_list"></div>
                      </font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter lesson position" value="{{old('position')}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('lesson.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

   

    </section>
@endsection

@section('script')
<script>



  $(function () {
    var schoolId = "";
    var gId = "";
    var tId = "";
    if($('#school_id').val()){
      schoolId = $('#school_id').val();
    }
    if($('#gid').val()){
      gId = $('#gid').val();
    }

    if($('#tid').val()){
      tId = $('#tid').val();
    }
    $.get("{{ route('lesson.grade-list') }}", 
      { 'school_id': schoolId }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        if(len>0){
            console.log(data['gradeList']);
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var oldGradeId = "";
                if(gId == id)
                {
                  oldGradeId = "selected";
                }
                var option = "<option value='"+id+"' "+oldGradeId+">"+name+"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
    console.log('term id',tId);
    //term List
    $.get("{{ route('lesson.term-list') }}", 
      { 'grade_id': gId }, 
      function( data ) {
        $("#term_id option").remove();
        var len = data['termList'].length;
        if(len>0){
          var option = "<option value=''>--select--</option>";
            console.log(data['termList']);
            for(var i=0; i<len; i++){
                var id = data['termList'][i].id;
                var name = data['termList'][i].term_name;
                var oldTermId = "";
                if(tId == id)
                {
                  oldTermId = "selected";
                }
                var option = "<option value='"+id+"' "+oldTermId+">"+name+"</option>";
                $("#term_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#term_id").append(option); 
        }
      }
    );

  });
  
  $('#school_id').change(function(e) 
  {
      $.get("{{ route('lesson.grade-list') }}", 
      { 'school_id': $(this).val() }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        
        if(len>0){
          var option = "<option value=''>--select--</option>";
          $("#grade_id").append(option); 
            // console.log(data['gradeList']);
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
  });

  //term list

  $('#grade_id').change(function(e) 
  {
      $.get("{{ route('lesson.term-list') }}", 
      { 'grade_id': $(this).val() }, 
      function( data ) {
        $("#term_id option").remove();
        var len = data['termList'].length;
        if(len>0){
          var option = "<option value=''>--select--</option>";
          $("#term_id").append(option); 
            console.log(data['termList']);
            for(var i=0; i<len; i++){
                var id = data['termList'][i].id;
                var name = data['termList'][i].term_name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#term_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#term_id").append(option); 
        }
      }
    );
  });
  jQuery('#term_id').change(function(e) {
      jQuery.get('{{ route('lesson.position-list') }}', 
      { 'term_id': $(this).val() }, 
      function( data ) {
        $('#position_list').empty();
        var positionLen =  data['position'].length;
        var positions =[];  
        console.log(data['position']);
            for(var j=0; j<positionLen;j++)
            {
              positions.push(data['position'][j].position);
            }
            var position = " Occupied Positon :-"+positions+"";
            $('#position_list').append(position);
        
      }
    );
  });
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
      term_id: {
        required: true,
      },
      title: {
        required: true,
      },
      position: {
        required: true,
      },
      short_description: {
        required: true,
      },
      lesson_name: {
        required: true,
      },
      'resource[0][description]': {
        required: true,
      },
     
      how_do_they_learn: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
      what_are_the_learing_outcome: {
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
      assignment:{
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
      
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
      term_id: {
        required: "Please select term",
      },
      title: {
        required: "Please enter what do they learn",
      },
      lesson_name: {
        required: "Please enter lesson name",
      },
      position: {
        required: "Please enter lesson position",
      },
      short_description: {
        required: "Please enter lesson short description",
      },
      how_do_they_learn:{
        required: "Please enter how do they learn",
      },
      what_are_the_learing_outcome:{
        required: "Please enter what are the learing outcome",
      },
      assignment:{
        required: "Please enter assignment",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
});

</script>
@endsection