@extends('admin.layouts.app')

 @section('content_header')
Lessons
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-lesson') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-7">
                    <h5>{{$lesson ->lesson_name}}</h5>
                  </div>
                  <div class="col-5">
                  
                    <a class="btn btn-default" href="{{route('lesson.index')}}" >Back</a>
                    <a class="btn btn-default mx-2" href="{{route('lesson.submission-list',$lesson->id)}}" >Submissions</a>
                  </div>
                </div>
              </div>
              <!-- form start -->
              <form id="quickForm" action="{{route('lesson.update',$lesson->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <!-- select -->
                  <div class="form-group">
                    <label for="school">School</label>
                      <select class="form-control" id="school_id" name="school_id">
                        <option value="{{$lesson['term']['grade']['school_id']}}">{{$lesson['term']['grade']['school']['name']}}</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label>Grade</label>
                    <select class="form-control" id="grade_id" name="grade_id">
                      <option value="{{$lesson['term']['grade']['id']}}">{{$lesson['term']['grade']['grade_name']}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Term</label>
                    <select class="form-control" id="term_id" name="term_id">
                      <option value="{{$lesson['term']['id']}}">{{$lesson['term']['term_name']}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="title">What Do They Learn</label>
                      <input type="text" name="title" class="form-control" id="title" placeholder="Enter what do they learn" value="{{print_value(old('title'),$lesson->title)}}">
                      @error('title')
                        <span style="color:red">{{ $message }}</span>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="term_name">Lesson Name</label>
                    <input type="text" name="lesson_name" class="form-control" id="lesson_name" placeholder="Enter lesson name" value="{{print_value(old('lesson_name'),$lesson->lesson_name)}}">
                    @error('lesson_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="short_description">Short Description</label>
                    <textarea name="short_description" id="short_description" class="form-control">{{print_value(old('short_description'),$lesson->short_description)}}</textarea>
                    @error('short_description')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="how_do_they_learn">How do they Learn</label>
                    <textarea name="how_do_they_learn" id="editor1" class="form-control ckeditor" >{{print_value(old('how_do_they_learn'),$lesson->how_do_they_learn)}}</textarea>
                    @error('how_do_they_learn')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="what_are_the_learing_outcome">What are the learning outcome</label>
                    <textarea name="what_are_the_learing_outcome" id="editor2" class="form-control ckeditor" >{{print_value(old('what_are_the_learing_outcome'),$lesson->what_are_the_learing_outcome)}}</textarea>
                    @error('what_are_the_learing_outcome')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="assignment">Assignment</label>
                    <textarea name="assignment" id="editor2" class="form-control ckeditor" >{{print_value(old('assignment'),$lesson->assignment)}}</textarea>
                    @error('assignment')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                        <font color="red">Occupied Positon :- @foreach($position as $position) {{$position->position}} @endforeach</font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter lesson position" value="{{print_value(old('position'),$lesson->position)}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('lesson.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

    </section>
@endsection

@section('script')
<script>
  

$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
      term_id: {
        required: true,
      },
      title: {
        required: true,
      },
      lesson_name: {
        required: true,
      },
      position: {
        required: true,
      },
      short_description: {
        required: true,
      },
      'resource[0][description]': {
        required: true,
      },
      how_do_they_learn: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
      what_are_the_learing_outcome: {
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
      assignment:{
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
      term_id: {
        required: "Please select term",
      },
      title: {
        required: "Please enter what do they learn",
      },
      lesson_name: {
        required: "Please enter lesson name",
      },
      position: {
        required: "Please enter lesson position",
      },
      short_description: {
        required: "Please enter lesson short description",
      },
      how_do_they_learn:{
        required: "Please enter how do they learn",
      },
      what_are_the_learing_outcome:{
        required: "Please enter what are the learing outcome",
      },
      assignment:{
        required: "Please enter assignment",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
$(function () {
  $('#quickForm2').validate({
    ignore: [],
    rules: {
      assignment: {
        required: function() 
        {
          return CKEDITOR.instances.editor2.updateElement();
        },
      },
    },
    messages: {
      assignment:{
        required: "Please enter assignment",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
});
</script>
@endsection