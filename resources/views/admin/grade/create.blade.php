@extends('admin.layouts.app')

 @section('content_header')
Grades
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-grade') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('grade.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                       <!-- select -->
                   <div class="form-group">
                        <label for="school">School</label>
                        <select class="form-control" id="school_id" name="school_id">
                          <option value="">--select--</option>
                         @foreach($schoolLists as $school)
                          <option value="{{$school->id}}" @if(old('school_id') == $school->id) selected @endif>{{$school->name}}</option>
                         @endforeach
                        </select>
                      @error('school_id')
                      <span style="color:red">{{ $message }}</span>
                      @enderror
                  </div>
                  <div class="form-group">
                  <label>Teacher</label>
                  <select class="select2" multiple="multiple"  style="width: 100%;" id="teacher_id" name="teacher_id[]">
                  
                  </select>
                  @error('teacher_id')
                      <span style="color:red">{{ $message }}</span>
                  @enderror
                  <input id="tid" type="hidden" value="{{old('teacher_id[]')}}"/>
                </div>
                  <div class="form-group">
                    <label for="grade_name">Grade Name</label>
                    <input type="text" name="grade_name" class="form-control" id="grade_name" placeholder="Enter grade name" value="{{old('grade_name')}}">
                    @error('grade_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                      <font color="red">
                        <div id="position_list"></div>
                      </font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter grade position" value="{{old('position')}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('grade.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
   $(function () {
    var schoolId = "";
    var tId = [];
    if($('#school_id').val()){
      schoolId = $('#school_id').val();
      
    }
    if($('#tid').val()){
      tId = $('#tid').val();
      
    }
    console.log('teacher id'+$('#tid').val());
    $.get("{{ route('grade.teacher-list') }}", 
      { 'school_id': schoolId }, 
      function( data ) {
        $("#teacher_id option").remove();
        $('#position_list').empty();
        var len = data['teacherList'].length;
        var positionLen =  data['position'].length;
        var positions =[];  
            console.log(len);
            for(var i=0; i<len; i++){
                var id = data['teacherList'][i].id;
                var name = data['teacherList'][i].name;
                var oldTeacherId = "";
                if(tId == id)
                {
                  oldTeacherId = "selected";
                }
                var option = "<option value='"+id+"' "+oldTeacherId+">"+name+"</option>";
                $("#teacher_id").append(option); 
            }
            for(var j=0; j<positionLen;j++)
            {
              console.log('inside for loop');
              positions.push(data['position'][j].position);
            }
            var position = " Occupied Positon :-"+positions+"";
            $('#position_list').append(position);
      }
    );
  });
  jQuery('#school_id').change(function(e) {
      jQuery.get('{{ route('grade.teacher-list') }}', 
      { 'school_id': $(this).val() }, 
      function( data ) {
        $("#teacher_id option").remove();
        $('#position_list').empty();
        var len = data['teacherList'].length;
        var positionLen =  data['position'].length;
        var positions =[];  
            for(var i=0; i<len; i++){
                var id = data['teacherList'][i].id;
                var name = data['teacherList'][i].name;
                var option = "<option value='"+id+"'>"+name+"</option>";
                $("#teacher_id").append(option); 
            }
            for(var j=0; j<positionLen;j++)
            {
              console.log('inside for loop');
              positions.push(data['position'][j].position);
            }
            var position = " Occupied Positon :-"+positions+"";
            $('#position_list').append(position);
        
      }
    );
  });

$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      school_id: {
        required: true,
      },
      'teacher_id[]':{
        required: true,
      },
      grade_name: {
        required: true,
      },
      position: {
        required: true,
      },
    
    },
    messages: {
      school_id: {
        required: "Please select school",
      },
      'teacher_id[]': {
        required: "Please select teachers",
      },
      grade_name: {
        required: "Please enter grade name",
      },
      position: {
        required: "Please enter grade position",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection