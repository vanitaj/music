@extends('admin.layouts.app')

 @section('content_header')
Grades
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-grade') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('grade.update',$grade->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                       <!-- select -->
                   <div class="form-group">
                        <label for="school">School</label>
                        <select class="form-control" id="school_id" name="school_id">
                          <option value="{{$grade['school']['id']}}">{{$grade['school']['name']}}</option>
                        </select>
                        @error('school_id')
                          <span style="color:red">{{ $message }}</span>
                        @enderror
                  </div>
                  <!-- <div class="form-group">
                        <label for="school">Teacher</label>
                        <select class="form-control" id="teacher_id" name="teacher_id">
                          <option value="">--select--</option>
                        </select>
                  </div> -->
                  <div class="form-group">
                  <label>Teacher</label>
                  <select class="select2" multiple="multiple"  style="width: 100%;" id="teacher_id" name="teacher_id[]">
                  @foreach($teacherList as $teacher)
                    <option value="{{$teacher['id']}}" @if($grade['teacher']->contains($teacher->id))) selected @endif>{{$teacher['name']}}</option>
                   @endforeach
                  </select>
                  @error('teacher_id')
                    <span style="color:red">{{ $message }}</span>
                  @enderror
                </div>
                  <div class="form-group">
                    <label for="grade_name">Grade Name</label>
                    <input type="text" name="grade_name" class="form-control" id="grade_name" placeholder="Enter grade name" value="{{print_value(old('grade_name'),$grade->grade_name)}}">
                    @error('grade_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                        <font color="red">Occupied Positon :- @foreach($position as $position) {{$position->position}} @endforeach</font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter grade position" value="{{print_value(old('position'),$grade->position)}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('grade.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      school_id: {
        required: true,
      },
      'teacher_id[]':{
        required: true,
      },
      grade_name: {
        required: true,
      },
    
    },
    messages: {
      school_id: {
        required: "Please select school",
      },
      'teacher_id[]': {
        required: "Please select teachers",
      },
      grade_name: {
        required: "Please enter grade name",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection