@extends('admin.auth.auth_app')

@section('content')
<p class="login-box-msg">You are only one step a way from your new password, recover your password now.</p>
<form action="{{route('admin.reset.password.post')}}" method="post">
@csrf
@php
    if (!isset($token)) {
        $token = \Request::route('token');
    }
@endphp
<input type="hidden" name="token" value="{{ $token }}">
<div class="input-group mb-3">
  <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
  <div class="input-group-append">
    <div class="input-group-text">
      <span class="fas fa-envelope"></span>
    </div>
  </div>
</div>
<div class="input-group mb-3">
  <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
  <div class="input-group-append">
    <div class="input-group-text">
      <span class="fas fa-lock"></span>
    </div>
  </div>
</div>
<div class="input-group mb-3">
  <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm Password">
  <div class="input-group-append">
    <div class="input-group-text">
      <span class="fas fa-lock"></span>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <button type="submit" class="btn btn-primary btn-block">Change password</button>
  </div>
  <!-- /.col -->
</div>
</form>

<p class="mt-3 mb-1">
<a href="{{ route('admin.login') }}">Login</a>
</p>
@endsection