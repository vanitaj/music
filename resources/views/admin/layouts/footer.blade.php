<!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; <script>
          document.write('2023 - ' + new Date().getFullYear());
          // document.write(new Date().getFullYear());
        </script> </strong>
    Developed by Mutefrog Technologies<img src="{{ asset('admin/images/mf logo.png')}}" alt="logo" style="height: 15px;margin-left: 5px;">.
    <!-- <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.2.0
    </div> -->
  </footer>
