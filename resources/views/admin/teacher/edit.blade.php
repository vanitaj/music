@extends('admin.layouts.app')

@section('content_header')
Teachers
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-teacher') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('teachers.update',$teacher->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
               
                    <!-- select -->
                   <div class="form-group">
                        <label for="school">School</label>
                        <select class="form-control" name="school_id">
                          <option value="{{$teacher['school']['id']}}">{{$teacher['school']['name']}}</option>
                        </select>
                  </div>
                  <div class="form-group">
                    <label for="name">Teacher Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter  name" value="{{print_value(old('name'),$teacher->name)}}">
                    @error('name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="email">Teacher Email</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="{{print_value(old('email'),$teacher->email)}}" readonly>
                    @error('email')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="phone_no">Teacher Phone No</label>
                    <input type="text" name="phone_no" class="form-control" id="phone_no" placeholder="Enter teacher phone no" value="{{print_value(old('phone_no'),$teacher->phone_no)}}">
                    @error('phone_no')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label id="date">Teacher DOB</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control float-right" id="date1" name="dob" value="{{print_value(old('dob'),$teacher->dob)}}">
                     
                    </div>
                    @error('dob')
                        <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="designation">Teacher Designation</label>
                    <input type="text" name="designation" class="form-control" id="designation" placeholder="Enter teacher designation" value="{{print_value(old('designation'),$teacher->designation)}}">
                    @error('designation')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  
                  <div class="form-group">
                    <label>Teacher Joining Date</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control float-right" id="date2" name="joining_date" value="{{print_value(old('joining_date'),$teacher->joining_date)}}">
                      @error('joining_date')
                        <span style="color:red">{{ $message }}</span>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Enter password" value="{{old('password')}}">
                    @error('password')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Enter confirm password"  value="{{old('c_password')}}">
                    @error('password_confirmation')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                   
                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                      <input type="checkbox" class="custom-control-input" id="customSwitch3" name="status" @checked(old('status',$teacher->status)) value="1">
                      <label class="custom-control-label" for="customSwitch3">Status</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('teachers.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>

$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      school_id: {
        required: true,
      },
      name:{
        required: true,
      },
      email:{
        required: true,
        email: true
      },
      dob:{
        required: true,
       
      },
      phone_no:{
        required: true,
      
      },
      designation:{
        required: true,
       
      },
      joining_date:{
        required: true,
       
      },
      password:{
        // required: true,
        minlength: 8,
      },
      password_confirmation:{
        // required: true,
        minlength: 8,
        equalTo: "#password",
      },
    },
    messages: {
      school_id: {
        required: "Please select a school",
      },
      name: {
        required: "Please enter a name",
      },
      email: {
        required: "Please enter a email",
      },
      phone_no: {
        required: "Please enter a teacher phone no",
      },
      designation: {
        required: "Please enter a  teacher designation",
      },
      password: {
        required: "Please enter a password",
      },
      password_confirmation: {
        required: "Please enter a confirm password",
        equalTo: "Confirm password does not match with password field."
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection