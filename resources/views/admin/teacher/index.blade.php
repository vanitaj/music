@extends('admin.layouts.app')
@section('content_header')
Teachers
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-teacher') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('teachers.create')}}">Add New</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Teacher Name</th>
                    <th>Teacher Email</th>
                    <th>Teacher Phone No</th>
                    <th>School Name</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($teacherLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->email}}</td>
                        <td>{{$row->phone_no}}</td>
                        <td>{{$row['school']['name']}}</td>
                        <td>{{ $row->status ? 'Active' : 'Not Active' }}</td>
                        <td>
                          <a class="btn btn-primary btn-sm" href="{{ route('teachers.edit',$row->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>                              
                          </a>
                          {{--<!-- <a class="btn btn-danger btn-sm delete-record delete-row" href="{{ route('teachers.destroy',$row->id) }}" data-id="{{$row->id}}">
                              <i class="fas fa-trash">
                              </i>
                          </a>
                          <form id="delete-form-{{$row->id}}" method="post" action="{{ route('teachers.destroy',$row->id) }}" display="none">
                            @csrf
                            @method('DELETE')
                          </form> -->--}}
                        </td>
                      </tr>    
                     @endforeach   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection