@extends('admin.layouts.app')
@section('content_header')
Contact Us
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-contact-us') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Teacher Name</th>
                    <th>Teacher Email</th>
                    <th>Teacher Phone no</th>
                    <th>School Name</th>
                    <th>Message</th>
                    <th>Received On</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($contactLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$row['teacher']['name']}}</td>
                        <td>{{$row['teacher']['email']}}</td>
                        <td>{{$row['teacher']['phone_no']}}</td>
                        <td>{{$row['teacher']['school']['name']}}</td>
                        <td> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg-{{$row->id}}">
                                View
                            </button>
                            <div class="modal fade" id="modal-lg-{{$row->id}}">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Message</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="control-label col-sm-4" for="name">Message:</label>
                                                <div class="col-sm-8">{{  $row->message }}</div>
                                            </div>
                                         
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div></td>
                        <td>{{formatted_date($row->created_at)}}</td>
                      </tr>    
                     @endforeach   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection