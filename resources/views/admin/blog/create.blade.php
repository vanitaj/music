@extends('admin.layouts.app')

 @section('content_header')
Blogs
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-blog') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('blogs.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Enter title" value="{{old('title')}}">
                    @error('title')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" class="form-control" id="slug" placeholder="slug" value="{{old('slug')}}">
                    @error('slug')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                   
                  <div class="form-group">
                    <label for="exampleInputFile">Banner Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="banner_image" name="banner_image" accept="image/png, image/gif, image/jpeg ,image/webp">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                       
                      </div>
                      
                    </div>
                    <small>
                        <font color="red">Please select image of size  x .</font>
                    </small>
                      @error('banner_image')
                        <span  style="color:red">{{ $message }}</span>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label>Blog Date</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control float-right" id="reservation" name="blog_date" value="{{old('blog_date')}}">
                      @error('blog_date')
                        <span style="color:red">{{ $message }}</span>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="min_of_read">Minutes Of Read</label>
                    <input type="text" name="min_of_read" class="form-control" id="min_of_read" placeholder="minutes of read" value="{{old('min_of_read')}}">
                    <small>
                        <font color="red">If minutes of read is 5 min enter 5 only.</font>
                    </small>
                    @error('min_of_read')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="editor1" class="form-control ckeditor" >{{ old('description') }}</textarea>
                    @error('description')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                      <input type="checkbox" class="custom-control-input" id="customSwitch3" name="status" @if (old('status') == 1) checked @endif value="1">
                      <label class="custom-control-label" for="customSwitch3">Status</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('blogs.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
  jQuery('#title').change(function(e) {
      jQuery.get('{{ route('blog.check_slug') }}', 
      { 'title': $(this).val() }, 
      function( data ) {
        jQuery('#slug').val(data.slug);
      }
    );
  });

$(function () {
  $.validator.addMethod('filesize', function (value, element, arg) {
        if(element.files[0].size<=arg){
            return true;
        }else{
            return false;
        }
  });
  //Date picker
  $('#reservation').daterangepicker({
    locale: {
      format: 'YYYY-MM-DD'
    },
    showDropdowns: true,
    singleDatePicker: true,
    });
    bsCustomFileInput.init();
  $('#quickForm').validate({
    ignore: [],
    rules: {
      title: {
        required: true,
      },
      min_of_read:{
        number: true,
        required: true,
      },
      slug: {
        required: true,
      },
      banner_image:{
        required: true,
        extension: "jpg,jpeg,png,webp,gif",
        filesize: 2097152,
      },
      blog_date:{
        required: true,
      },
      description: {
        required: function() 
        {
          return CKEDITOR.instances.editor1.updateElement();
        },
      },
      terms: {
        required: true
      },
    },
    messages: {
      title: {
        required: "Please enter a title",
      },
      slug: {
        required: "Please enter a slug",
      },
      banner_image: {
        required: "Please select image",
        extension:"please select image of type jpg,png,webp.",
        filesize:"maximun upload image size is 2mb.",
        
      },
      blog_date: {
        required: "Please select blog date",
      },
      min_of_read: {
        required: "Please enter minutes of read",
      },
      description: {
        required: "Please provide a description"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection