@extends('admin.layouts.app')
@section('content_header')
Blogs
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-blog') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('blogs.create')}}">Add New</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Banner Image</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($blogList as $blog)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$blog->title}}</td>
                        <td> <a href="{{ media_file($blog->banner_image)}}" target="_blank"><img class="list-img" src="{{ media_file($blog->banner_image)}}" style="width:50px;height:50px;"></a></td>
                        <td>{{formatted_date($blog->blog_date)}}</td>
                        <td>{{ $blog->status ? 'Active' : 'Not Active' }}</td>
                        <td>
                          <a class="btn btn-primary btn-sm" href="{{ route('blogs.edit',$blog->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>                              
                          </a>
                          <a class="btn btn-danger btn-sm delete-record delete-row" href="{{ route('blogs.destroy',$blog->id) }}" data-id="{{$blog->id}}">
                              <i class="fas fa-trash">
                              </i>
                          </a>
                          <form id="delete-form-{{$blog->id}}" method="post" action="{{ route('blogs.destroy',$blog->id) }}" display="none">
                            @csrf
                            @method('DELETE')
                          </form>
                        </td>
                      </tr>    
                     @endforeach   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection