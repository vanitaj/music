@extends('admin.layouts.app')

 @section('content_header')
Terms
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-term') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('term.update',$term->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                       <!-- select -->
                   <div class="form-group">
                        <label for="school">School</label>
                        <select class="form-control" id="school_id" name="school_id">
                          <option value="{{$term['grade']['school_id']}}">{{$term['grade']['school']['name']}}</option>
                        </select>
                  </div>
                  <div class="form-group">
                  <label>Grade</label>
                  <select class="form-control" id="grade_id" name="grade_id">
                    <option value="{{$term['grade_id']}}">{{$term['grade']['grade_name']}}</option>
                  </select>
                </div>
                  <div class="form-group">
                    <label for="term_name">Term Name</label>
                    <input type="text" name="term_name" class="form-control" id="term_name" placeholder="Enter term name" value="{{print_value(old('term_name'),$term->term_name)}}">
                    @error('term_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                        <font color="red">Occupied Positon :- @foreach($position as $position) {{$position->position}} @endforeach</font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter term position" value="{{print_value(old('position'),$term->position)}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('term.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
    //   'teacher_id[]':{
    //     required: true,
    //   },
      term_name: {
        required: true,
      },
      position: {
        required: true,
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
    //   'teacher_id[]': {
    //     required: "Please select teachers",
    //   },
      term_name: {
        required: "Please enter term name",
      },
      position: {
        required: "Please enter term position",
      },
      
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection