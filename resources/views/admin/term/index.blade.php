@extends('admin.layouts.app')
@section('content_header')
Terms
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-term') }}
@endsection
@section('main-content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-default" href="{{route('term.create')}}">Add New</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Term Name</th>
                    <th>Grade</th>
                    <th>School Name</th>
                    <th>Completed</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($termLists as $row)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$row->term_name}}</td>
                        <td>{{$row['grade']['grade_name']}}</td>
                        <td>{{$row['grade']['school']['name']}}</td>
                        <td>{{$row['is_complete'] ? 'Yes' : 'No' }}</td>
                        <td>
                          <a class="btn btn-primary btn-sm" href="{{ route('term.edit',$row->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>                              
                          </a>
                          <a class="btn btn-danger btn-sm delete-record delete-row" href="{{ route('term.destroy',$row->id) }}" data-id="{{$row->id}}">
                              <i class="fas fa-trash">
                              </i>
                          </a>
                          <form id="delete-form-{{$row->id}}" method="post" action="{{ route('term.destroy',$row->id) }}" display="none">
                            @csrf
                            @method('DELETE')
                          </form>
                        </td>
                      </tr>    
                     @endforeach   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
   
@endsection

@section('script')

@endsection