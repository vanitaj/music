@extends('admin.layouts.app')

 @section('content_header')
Terms
@endsection
@section('breadcrumb')
{{ Breadcrumbs::render('a-term') }}
@endsection
@section('main-content')
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- form start -->
              <form id="quickForm" action="{{route('term.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                       <!-- select -->
                   <div class="form-group">
                        <label for="school">School</label>
                        <select class="form-control" id="school_id" name="school_id">
                          <option value="">--select--</option>
                         @foreach($schoolLists as $school)
                          <option value="{{$school->id}}" @if(old('school_id') == $school->id) selected @endif>{{$school->name}}</option>
                         @endforeach
                        </select>
                  </div>
                  <div class="form-group">
                    <label  for="school">Grade</label>
                    <select class="form-control"   id="grade_id" name="grade_id">
                     <option value="">--select--</option>
                    </select>
                    <input id="gid" type="hidden" value="{{old('grade_id')}}"/>
                 </div>
                  <div class="form-group">
                    <label for="term_name">Term Name</label>
                    <input type="text" name="term_name" class="form-control" id="term_name" placeholder="Enter term name" value="{{old('term_name')}}">
                    @error('term_name')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="position">Position</label><br />
                    <small>
                      <font color="red">
                        <div id="position_list"></div>
                      </font>
                    </small>
                    <input type="number" name="position" class="form-control" id="position" placeholder="Enter term position" value="{{old('position')}}">
                    @error('position')
                      <span style="color:red">{{ $message }}</span>
                    @enderror
                  </div>
               
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('term.index') }}" class="btn btn-warning">Back</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('script')
<script>
  $(function () {
    var schoolId = "";    
    if($('#school_id').val()){
      schoolId = $('#school_id').val();
    }    
    getSchoolGrades(schoolId); 
  });
  
  $('#school_id').change(function(e) 
  {
    var schoolId = $(this).val();
    getSchoolGrades(schoolId);
  });

function getSchoolGrades(schoolId){
  var gId = "";
  if($('#gid').val()){
      gId = $('#gid').val();
    }   
    
  $.get("{{ route('term.grade-list') }}", 
      { 'school_id': schoolId }, 
      function( data ) {
        $("#grade_id option").remove();
        var len = data['gradeList'].length;
        if(len>0){
          var option = "<option value=''>--select--</option>";
          $("#grade_id").append(option); 
            for(var i=0; i<len; i++){
                var id = data['gradeList'][i].id;
                var name = data['gradeList'][i].grade_name;
                var oldGradeId = "";
                if(gId == id)
                {
                  oldGradeId = "selected";
                }
                var option = "<option value='"+ id +"' "+ oldGradeId +">"+ name +"</option>";
                $("#grade_id").append(option); 
            }
        }else
        {
            var option = "<option value=''>No record found</option>";
            $("#grade_id").append(option); 
        }
      }
    );
}
jQuery('#grade_id').change(function(e) {
      jQuery.get('{{ route('term.position-list') }}', 
      { 'grade_id': $(this).val() }, 
      function( data ) {
        $('#position_list').empty();
        var positionLen =  data['position'].length;
        var positions =[];  
        console.log(data['position']);
            for(var j=0; j<positionLen;j++)
            {
              positions.push(data['position'][j].position);
            }
            var position = " Occupied Positon :-"+positions+"";
            $('#position_list').append(position);
        
      }
    );
  });

$(function () {
  $('#quickForm').validate({
    ignore: [],
    rules: {
      grade_id: {
        required: true,
      },
      school_id: {
        required: true,
      },
    //   'teacher_id[]':{
    //     required: true,
    //   },
      term_name: {
        required: true,
      },
      position: {
        required: true,
      },
    
    },
    messages: {
      grade_id: {
        required: "Please select grade",
      },
      school_id: {
        required: "Please select school",
      },
    //   'teacher_id[]': {
    //     required: "Please select teachers",
    //   },
      term_name: {
        required: "Please enter term name",
      },
      position: {
        required: "Please enter term position",
      },
      
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection