<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    use HasFactory;
    public $table ='password_resets';
    const UPDATED_AT = null;

    protected $dates = [
        'created_at',
    ];
    protected $fillable = ['email', 'token'];
}
