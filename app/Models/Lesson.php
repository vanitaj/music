<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;
    protected $table = 'lessons';
    protected $fillable = [
        'term_id',
    	'lesson_name', 
        'how_do_they_learn',
        'what_are_the_learing_outcome',
        'position',
        'is_complete',
        'short_description',
        'feedback'
    ];
    public static function boot() 
    {
        parent::boot();

        Lesson::deleting(function($model) 
        {    
           $resource = Resource::where('lesson_id',$model->id)->get();
           foreach($resource as $resource)
           {
                media_delete($resource->url);
                $resource->delete();
           }
        });
    }
    public function term()
    {
        return $this->belongsTo('App\Models\Term'); 
    }
    public function resource()
    {
        return $this->hasMany('App\Models\Resource'); 
    }
    public function assignment()
    {
        return $this->hasMany('App\Models\Assignment'); 
    }
}
