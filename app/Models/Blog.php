<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Blog extends Model
{
    use HasFactory,Sluggable;
    protected $fillable = [
    	'banner_image', 
    	'title', 
    	'slug',
    	'description',
    	'blog_date',
    	'min_of_read',
    	'status',
    ];
	public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
