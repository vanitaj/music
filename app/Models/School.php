<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;
    protected $table = 'schools';
    protected $fillable = [
    	'school_code', 
    	'name', 
    	'address',
    	'school_initials',
    	'status',
        'official_contact_no'
    ];
	public function teacher()
    {
        return $this->hasMany('App\Models\Teacher'); 
    }

    public function grade()
    {
        return $this->hasMany('App\Models\Grade'); 
    }

}
