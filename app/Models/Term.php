<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    use HasFactory;
    protected $table = 'terms';
    protected $fillable = [
        'grade_id',
    	'term_name', 
        'position',
        'is_complete',
    ];
    public static function boot() 
    {
        parent::boot();

        Term::deleting(function($model) 
        {     
            $lesson = Lesson::where('term_id',$model->id)->delete();
            Assessment::where('term_id',$model->id)->delete();

        });
    }
    public function grade()
    {
        return $this->belongsTo('App\Models\Grade'); 
    }
    public function lesson()
    {
        return $this->hasMany('App\Models\Lesson'); 
    }
    public function assessment()
    {
        return $this->hasMany('App\Models\Assessment'); 
    }
 
}
