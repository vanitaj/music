<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    use HasFactory;
    protected $table = 'assessments';
    protected $fillable = [
        'term_id',
    	'url', 
        'description', 
    ];
    public function term()
    {
        return $this->belongsTo('App\Models\Term'); 
    }
}
