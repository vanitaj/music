<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;
    protected $table = 'teachers';
    protected $fillable = [
    	'school_id', 
    	'name', 
    	'email',
        'dob',
        'joining_date',
        'phone_no',
        'designation',
    	'password',
    	'status',
    ];
    protected $hidden = [
        'password',
    ];
    public function school()
    {
        return $this->belongsTo('App\Models\School'); 
    }
    public function grade()
    {
        return $this->belongsToMany(Grade::class, 'teacher_grade');
    }

    public function contactUs()
    {
        return $this->hasMany('App\Models\ContactUs'); 
    }

    public function assignment()
    {
        return $this->hasMany('App\Models\Assignment'); 
    }

}
