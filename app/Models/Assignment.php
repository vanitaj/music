<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;
    protected $table = 'assignments';
    protected $fillable = [
        'lesson_id',
    	'document_type', 
        'url',
        'description',
    ];
    
    public function lesson()
    {
        return $this->belongsTo('App\Models\Lesson'); 
    }
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher'); 
    }
}
