<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    use HasFactory;
    protected $table = 'grades';
    protected $fillable = [
        'school_id',
    	'grade_name', 
        'position'
    ];
    public static function boot() 
    {
        parent::boot();

        Grade::deleting(function($model) 
        {     
            TeacherGrade::where('grade_id',$model->id)->delete();
            Term::where('grade_id',$model->id)->delete();
        });
    }
    public function teacher()
    {
        return $this->belongsToMany(Teacher::class, 'teacher_grade');
    }
    public function school()
    {
        return $this->belongsTo('App\Models\School'); 
    }
    public function term()
    {
        return $this->hasMany('App\Models\Term'); 
    }
}
