<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    use HasFactory;
    protected $table = 'contact_us';
    protected $fillable = [
        'teacher_id',
        'message', 
    ];
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher'); 
    }
}
