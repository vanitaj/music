<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    use HasFactory;
    protected $table = 'resources';
    protected $fillable = [
        'lesson_id',
    	'document_type', 
        'url',
        'description',
    ];
    
    public function lesson()
    {
        return $this->belongsTo('App\Models\Lesson'); 
    }
    
}
