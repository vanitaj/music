<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GradeFormRequest;
use App\Models\Grade;
use App\Models\School;
use App\Models\Teacher;
use App\Models\TeacherGrade;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class GradeController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gradeLists = Grade::with('school')->latest()->get();
        //dd($gradeLists);
        return view('admin.grade.index',compact('gradeLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schoolLists = School::latest()->get();
        return view('admin.grade.create',compact('schoolLists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeFormRequest $request)
    {
        $this->storeGrade(new Grade(),$request);
        return $this->redirectToIndex('grade', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
        $grade = Grade::with(['school','teacher'])->findorFail($id);
        $teacherList = Teacher::where('school_id',$grade->school_id)->get();
        $position = Grade::select('position')->where('school_id',$grade->school_id)->orderBy('position', 'DESC')->get(); 
        return view('admin.grade.edit', compact('grade','teacherList','position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GradeFormRequest $request, $id)
    {
        $grade = Grade::findorFail($id);
        $this->storeGrade($grade,$request);
        return $this->redirectToIndex('grade', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grade = Grade::findorFail($id);
       
        $grade->delete();
        return $this->redirectToIndex('grade', $this->constants->get('constants.message.delete'));
    }

    public function storeGrade($grade,$data)
    {
        $grade->school_id   = $data->school_id;
        $grade->grade_name  = $data->grade_name;
        $grade->position  = $data->position;
        $grade->save();
        $grade->teacher()->sync($data->teacher_id);

    }

    public function teacherList(Request $request)
    {
        $teacherList = Teacher::where('school_id',$request->school_id)->get();
        $position = Grade::select('position')->where('school_id',$request->school_id)->orderBy('position', 'DESC')->get(); 
        return response()->json(['teacherList' => $teacherList,'position' =>$position]);
    }

    // public function validaFields($data,$id=null)
    // {

    //     $data->validate([
    //         'grade_name' => [
    //             'required',
    //              Rule::unique('grades')->where(function ($query)use($data) {
    //                  $query->where('grade_name', $data->grade_name)
    //                     ->where('school_id', $data->school_id);
    //              })->ignore($id)
    //         ],
    //     ]);
    // }
}
