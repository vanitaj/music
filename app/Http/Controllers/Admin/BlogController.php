<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\BlogFormRequest;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Models\Blog;

class BlogController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogList = Blog::latest()->get();
        return view('admin.blog.index',compact('blogList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogFormRequest $request)
    {
        // dd($request->all());
        $this->storeBlog(new Blog(),$request );
        return $this->redirectToIndex('blogs', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogs  = Blog::findorFail($id);
        return view('admin.blog.edit', compact('blogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogFormRequest $request, $id)
    {
        $blog = Blog::findorFail($id);
        $this->storeBlog( $blog ,$request );
        return $this->redirectToIndex('blogs', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::findorFail($id);
        media_delete($blog->banner_image);
        $blog->delete();
        return $this->redirectToIndex('blogs', $this->constants->get('constants.message.delete'));
    }

    public function storeBlog($blog , $data)
    {
        $blog->title            = $data->title;
        $blog->slug             = $data->slug;
        $blog->description      = $data->description;
        $blog->min_of_read      = $data->min_of_read;
        $blog->blog_date        = $data->blog_date;
        $blog->status           = $data->has('status') ? 1 : 0;
        if ($data->hasFile('banner_image')){            
            media_delete($blog->banner_image);
            $blog->banner_image = media_upload_compress($data->file('banner_image'), config('constants.uploads.banner'));
        }
        $blog->save();
        return;
    }
    public function checkSlug(Request $request)
    {
        // dd($request->all());
        $slug = SlugService::createSlug(Blog::class, 'slug', $request->title);
        return response()->json(['slug' => $slug]);
    }
}
