<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SchoolFormRequest;
use App\Models\School;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class SchoolController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schoolLists = School::latest()->get();
        return view('admin.school.index',compact('schoolLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.school.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolFormRequest $request)
    {
        $uniqueCode = $this->uniqueCode();
        $request->request->add(['school_code' => $uniqueCode]);
        $this->storeSchool(new School(),$request );
        return $this->redirectToIndex('schools', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school  = School::findorFail($id);
        return view('admin.school.edit', compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolFormRequest $request, $id)
    {
        $school = School::findorFail($id);
        $this->storeSchool( $school ,$request );
        return $this->redirectToIndex('schools', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        School::findorFail($id)->delete();
        return $this->redirectToIndex('schools', $this->constants->get('constants.message.delete'));
    }

    public function storeSchool($school , $data)
    {
        $school->name             = $data->name;
        $school->school_code      = $data->school_code;
        $school->address          = $data->address;
        $school->official_contact_no = $data->official_contact_no;
        //$school->school_initials  = $data->school_initials;
        $school->status           = $data->has('status') ? 1 : 0;
        $school->save();
        return;
    }

    public function uniqueCode()
    {
        $uniqueCode = Str::random(6);
        $code = School::where('school_code',$uniqueCode)->get();
      
        if(count($code)){
            return $this->uniqueCode();
        }
        return $uniqueCode;
    }
}
