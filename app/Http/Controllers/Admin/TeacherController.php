<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TeacherFormRequest;
use App\Models\School;
use App\Models\Teacher;
use App\Notifications\Admin\TeacherNotification;
use Notification;
use Illuminate\Http\Request;

class TeacherController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $teacherLists = Teacher::with('school')->latest()->get();
        return view('admin.teacher.index',compact('teacherLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schoolLists = School::latest()->get();
        return view('admin.teacher.create',compact('schoolLists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherFormRequest $request)
    {
        // dd($request->all());
        $this->storeTeacher(new Teacher(),$request );
        return $this->redirectToIndex('teachers', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher  = Teacher::with('school')->findorFail($id);
        return view('admin.teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherFormRequest $request, $id)
    {
        // dd($request->all());
        $teacher = Teacher::findorFail($id);
        $this->storeTeacher( $teacher ,$request );
        return $this->redirectToIndex('teachers', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::findorFail($id)->delete();
        return $this->redirectToIndex('teachers', $this->constants->get('constants.message.delete'));
    }

    public function storeTeacher($teacher , $data)
    {
        $teacher->name       = $data->name;
        $teacher->school_id  = $data->school_id;
        $teacher->email      = $data->email;

        $teacher->dob           = $data->dob;
        $teacher->joining_date  = $data->joining_date;
        $teacher->phone_no      = $data->phone_no;
        $teacher->designation   = $data->designation;
        $teacher->status        = $data->has('status') ? 1 : 0;
        
        if($data->password){
            $teacher->password   = bcrypt($data->password);
            Notification::route('mail', $data->email)->notify(new TeacherNotification ($data));
        }
        $teacher->save();
        return;
    }
}
