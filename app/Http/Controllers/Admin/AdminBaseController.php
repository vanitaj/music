<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminBaseController extends Controller
{
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	protected $constants;

	public function __construct()
    {
        $this->middleware('auth:admin');
        $this->constants = config();
    }
    

    public function redirectToIndex($indexPage, $message = '')
    {
        return redirect()->route("{$indexPage}.index")->with([
            'message' => $message
        ]);
    }

    public function responseToJson($message = '')
    {
        return response()->json([
            'message' => $message
        ]);
    }

    public function redirectToIndexWithError($indexPage, $message = '')
    {
        return redirect()->route("{$indexPage}.index")->with([
            'error' => $message
        ]);
    } 
   
}
