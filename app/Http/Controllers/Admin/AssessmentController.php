<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AssessmentFormRequest;
use App\Models\Assessment;
use App\Models\Grade;
use App\Models\School;
use App\Models\Term;
use Illuminate\Http\Request;

class AssessmentController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assessmentLists = Assessment::with('term.grade.school')->latest()->get();
        return view('admin.assessment.index',compact('assessmentLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schoolLists = School::latest()->get();
        return view('admin.assessment.create',compact('schoolLists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssessmentFormRequest $request)
    {
        $this->storeAssessment(new Assessment(),$request);
        return $this->redirectToIndex('assessment', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assessment = Assessment::with('term.grade.school')->findorFail($id);
        return view('admin.assessment.edit', compact('assessment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AssessmentFormRequest $request, $id)
    {
        $assessment = Assessment::findorFail($id);
        $this->storeAssessment($assessment,$request);
        return $this->redirectToIndex('assessment', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assessment = Assessment::findorFail($id);
        $assessment->delete();
        return $this->redirectToIndex('assessment', $this->constants->get('constants.message.delete'));
    }

    public function storeAssessment($assessment,$data)
    {
        $assessment->term_id        = $data->term_id;
        $assessment->url            = $data->url;
        $assessment->description    = $data->description;
        $assessment->save();
    }

    public function gradeList(Request $request)
    {
        $gradeLists = Grade::where('school_id',$request->school_id)->get();
        return response()->json(['gradeList' => $gradeLists]);
    }

    public function termList(Request $request)
    {
        $termLists = Term::where('grade_id',$request->grade_id)->get();
        return response()->json(['termList' => $termLists]);
    }
}
