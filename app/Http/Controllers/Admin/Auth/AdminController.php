<?php
namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Blog;
use App\Models\User;
use App\Models\HomeVideo;
use App\Models\Subscription;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id ());
        $userCount = User::where('role','0')->get();
        foreach($userCount as $user)
        {
            $subscription = $this->getSubscriptionInfo( $user );
            $user->subscription_type = $subscription->type;
        }
        $blogCount = Blog::all();
        $videoCount = HomeVideo::all();
        return view('admin.dashboard', compact('user','userCount','blogCount','videoCount'));
        //return view('admin.dashboard');
    }

    public function getSubscriptionInfo( $user )
    {                
        
        if( $user != null ){            
            $subscription = Subscription::select('transaction_date as subscription_start_date')
            ->where ( 'user_id', $user->id )
            ->where ( 'transaction_status', 1 )
            ->orderBy ( 'transaction_date', 'DESC' )
            ->first ();

            if($subscription == null){
                $subscription = new Subscription();
                $subscription->type = 'Free';
            }else{
                $subscription->type = 'Premium';         
            }
         
            return  $subscription;
        }
    }
}