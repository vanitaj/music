<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Admin;
use App\Models\PasswordReset;
use App\Notifications\Admin\AdminForgotPasswordNotification;
use Carbon\Carbon;
use Auth;
use Mail;
use Notification;

class PasswordController extends Controller
{
    public function showForgetPasswordForm()
    {
       return view('admin.auth.forget_password_form');
    }

    public function sendPasswordResetToken(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:admins',
        ]);

        $token = Str::random(64);

        PasswordReset::create([
            'email' => $request->email,
            'token' => $token, 
            //'created_at' => Carbon::now()
        ]);             
        if( $this->sendEmail ( $token, $request ) ){
            return back()->with('message', 'Password recovery link has been sent to your email address.');
        }
        return redirect()->back()->with('error', 'Oops! You have entered invalid credentials');
    }

    public function showPasswordResetForm($token)
    {
       return view('admin.auth.reset')->with('token', $token);
    }

    public function resetPassword(Request $request)
    {       
        $request->validate([
            'email' => 'required|email|exists:admins',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);        
    
        $updatePassword = PasswordReset::where([
                              'email' => $request->email, 
                              'token' => $request->token
                            ])
                            ->first();            

        if(!$updatePassword){
            return back()->withInput()->with('error', 'No user found with this email.');
        }else{
          if($updatePassword->created_at < Carbon::now()->subHours(2)){
            
            PasswordReset::where(['email'=> $request->email])->delete();
            return back()->withInput()->with('error', 'Invalid token!');
          }
       
          $user = Admin::where('email', $request->email)
                      ->update(['password' => Hash::make($request->password)]);

          PasswordReset::where(['email'=> $request->email])->delete();

          return redirect('admin/login')->with('message', 'Your password has been changed!');
        }
    }

    private function sendEmail($token, $user) {

        Notification::route('mail', $user->email)->notify(new AdminForgotPasswordNotification ($user,$token));
        // Mail::send ( 'admin.auth.verify', [ 
        //         'user' => $user->email,
        //         'token' => $token 
        // ], function ($message) use ($user) {      
        //     $message->from (config('constants.mail.mailFrom') );
        //     $message->to ( $user->email );
        //     $message->subject ( 'Reset Password Notification' );
        // } );
        return $user;
    }
}
