<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonFormRequest;
use App\Models\Assignment;
use App\Models\Grade;
use App\Models\Lesson;
use App\Models\Resource;
use App\Models\School;
use App\Models\Term;
use Illuminate\Http\Request;
use DB;
use Exception;

class LessonController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessonLists = Lesson::with(['term','term.grade.school'])->latest()->get();
        // dd($lessonLists);
        return view('admin.lesson.index',compact('lessonLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lessonId = null;
        $schoolLists = School::latest()->get();
        return view('admin.lesson.create',compact('schoolLists' ,'lessonId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LessonFormRequest $request)
    {
        // dd($request->all());
        $lesson = $this->storeLesson(new Lesson(),$request);
        return $this->redirectToIndex('lesson', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson = Lesson::with(['term','term.grade.school'])->findorFail($id);
        $position = Lesson::select('position')->where('term_id',$lesson->term_id)->orderBy('position', 'DESC')->get(); 
        return view('admin.lesson.edit', compact('lesson','position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LessonFormRequest $request, $id)
    {
        $lesson = Lesson::findorFail($id);
        $this->storeLesson($lesson,$request);
        return $this->redirectToIndex('lesson', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson = Lesson::findorFail($id);
        $lesson->delete();
        return $this->redirectToIndex('lesson', $this->constants->get('constants.message.delete'));
    }

    public function storeLesson($lesson,$data)
    {
            $lesson->term_id                       = $data->term_id;
            $lesson->title                         = $data->title;
            $lesson->lesson_name                   = $data->lesson_name;
            $lesson->how_do_they_learn             = $data->how_do_they_learn;
            $lesson->what_are_the_learing_outcome  = $data->what_are_the_learing_outcome;
            $lesson->position                      = $data->position;
            $lesson->assignment                    = $data->assignment;
            $lesson->short_description             = $data->short_description;
            $lesson->feedback                      = $data->feedback;
            // $lesson->is_complete                   = $data->has('is_complete') ? 1 : 0;
            $lesson->save();
             
            return   $lesson; 
    }

    public function submissionList($id)
    {   
        $lesson = Lesson::findorFail($id);
        $submissionLists = Assignment::with('teacher')->where('lesson_id',$id)->get();
        return view('admin.lesson.submission',compact('submissionLists','lesson'));

    }

    public function submissionDownload($id)
    {
        $assignment = Assignment::findorFail($id);
        return response()->download(public_path($assignment->url));

    }
    public function storeResource(Request $request)
    {
        $resourceData = Resource::where('lesson_id',$request->lessonId)->get();
        if($request->resource){

            foreach($resourceData as $row)
            {
                    $search = collect($request->resource)->where('resource_id',$row->id)->toArray();
                    if(!$search)
                    {
                       media_delete($row->url);
                       $row->delete();
                    }
            }
            
            foreach($request->resource as $key=>$resources){
                if($resources['type'] == 'Upload Document' && isset($resources['url'])) 
                {
                    $url = media_upload($resources['url'], config('constants.uploads.resource').'/'.$request->lessonId);
                }else if(isset($resources['url'])){
                    $url = $resources['url'];
                }

                if(isset($resources['resource_id'])){
                    $resource = Resource::findorFail($resources['resource_id']);
                }else
                    $resource = new Resource();
                
                $resource->lesson_id = $request->lessonId;
                $resource->document_type = $resources['type'];
                if(isset($url))
                    $resource->url = $url;
                $resource->description = $resources['description'];
                $resource->save();
            }

        }
        else
        {
            foreach($resourceData as $row)
            {
                media_delete($row->url);
                $row->delete();
            }
        }
        return redirect()->back()->with(['message'=> 'Resource save successfully','lessonId'=> $request->lessonId]);
    }

    public function storeAssignment(Request $request)
    {
        $request->validate([
            'assignment'                         =>  'required',
        ]);
        Lesson::where('id',$request->lessonId)->update([
            'assignment'=>$request->assignment
        ]);
        
        return redirect()->back()->with(['message'=> 'Assignment save successfully','lessonId'=> $request->lessonId]);
    }
    public function gradeList(Request $request)
    {
        $gradeLists = Grade::where('school_id',$request->school_id)->get();
        return response()->json(['gradeList' => $gradeLists]);
    }

    public function termList(Request $request)
    {
        $termLists = Term::where('grade_id',$request->grade_id)->get();
        return response()->json(['termList' => $termLists]);
    }

    public function positionList(Request $request)
    {
        $position = Lesson::select('position')->where('term_id',$request->term_id)->orderBy('position', 'DESC')->get(); 
        return response()->json(['position' =>$position]);
    }

    
}
