<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TermFormRequest;
use App\Models\Grade;
use App\Models\School;
use App\Models\Term;
use Illuminate\Http\Request;

class TermController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $termLists = Term::with(['grade','grade.school'])->latest()->get();
        // dd($termLists);
        return view('admin.term.index',compact('termLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schoolLists = School::latest()->get();
        return view('admin.term.create',compact('schoolLists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TermFormRequest $request)
    {
        $this->storeTerm(new Term(),$request);
        return $this->redirectToIndex('term', $this->constants->get('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $term = Term::with(['grade','grade.school'])->findorFail($id);
        $position = Term::select('position')->where('grade_id',$term->grade_id)->orderBy('position', 'DESC')->get(); 
        return view('admin.term.edit', compact('term','position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TermFormRequest $request, $id)
    {
        $term = Term::findorFail($id);
        $this->storeTerm($term,$request);
        return $this->redirectToIndex('term', $this->constants->get('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $term = Term::findorFail($id);
        $term->delete();
        return $this->redirectToIndex('term', $this->constants->get('constants.message.delete'));
    }

    public function storeTerm($term,$data)
    {
        $term->grade_id   = $data->grade_id;
        $term->term_name  = $data->term_name;
        $term->position  = $data->position;
        $term->is_complete    = $data->has('is_complete') ? 1 : 0;
        $term->save();
    }

    public function gradeList(Request $request)
    {
        $gradeList = Grade::where('school_id',$request->school_id)->get();
        return response()->json(['gradeList' => $gradeList]);
    }
    public function positionList(Request $request)
    {
        $position = Term::select('position')->where('grade_id',$request->grade_id)->orderBy('position', 'DESC')->get(); 
        return response()->json(['position' =>$position]);

    }
}
