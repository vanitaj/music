<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AssessmentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
          
            'term_id' => [
                'required',
                Rule::unique('assessments')->where(function ($query) {
                    $query->where('term_id', $this->term_id);
                })->ignore($this->route('assessment'))
            ],  
            'url' => 'required',
            'description' => 'required',
        ];                                                              
        return $rules;
    }

    public function messages()
    {
        return [
            'term_id.unique'              => 'Assessment already exists for this term.',
        ];
    }
}
