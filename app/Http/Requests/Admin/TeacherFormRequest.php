<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TeacherFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'school_id' => 'required',
            'name'      => 'required',
            'dob'       => 'required',
            'joining_date'  => 'required',
            'designation'   => 'required',
            'phone_no'      => 'required',
            
        ];
        if ($this->getMethod() == 'POST') {
            $rules += [
              
                'email'     => 'required|email|unique:teachers',
                'password'  => 'required|min:8|confirmed',
            ];
        }
        elseif ($this->getMethod() == 'PUT') {
            $rules += [
                'email'     => 'required|email|unique:teachers,email,'.$this->route('teacher'),
                'password'  => 'nullable|min:8|confirmed',
            ];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'school_id.required'   => 'Please select school.',
        ];
    }
}
