<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GradeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'school_id'   => 'required',
            'grade_name' => [
                'required',
                 Rule::unique('grades')->where(function ($query) {
                     $query->where('grade_name', $this->grade_name)
                        ->where('school_id', $this->school_id);
                 })->ignore($this->route('grade'))
            ],
                                                                             
        ];       
        if ($this->getMethod() == 'POST') {
            $rules += [
                'position'                  => 'required|unique:grades,position',
            ];
        }
        elseif ($this->getMethod() == 'PUT') {
            $rules += [
                'position'                  => 'required|unique:grades,position,'.$this->route('grade'),
            ];
        }
        
        return $rules;
       
    }
}
