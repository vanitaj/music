<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TermFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // $rules = [
        //     'grade_id' => 'required',
        //     'term_name'      => 'required',
            
        // ];
        // return $rules;
        $rules = [
            'grade_id' => 'required',
            'term_name' => [
                'required',
                 Rule::unique('terms')->where(function ($query) {
                     $query->where('term_name', $this->term_name)
                        ->where('grade_id', $this->grade_id);
                 })->ignore($this->route('term'))
            ],
                                                                             
        ];       
        if ($this->getMethod() == 'POST') {
            $rules += [
                'position'                  => 'required|unique:terms,position',
            ];
        }
        elseif ($this->getMethod() == 'PUT') {
            $rules += [
                'position'                  => 'required|unique:terms,position,'.$this->route('term'),
            ];
        }
        
        return $rules;
    }
}
