<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BlogFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'title'                 => 'required',
           
            'description'           => 'required',
            // 'banner_image'          => 'required',
            'blog_date'             => 'required',
            'min_of_read'           => 'required|numeric',
        ];
        if ($this->getMethod() == 'POST') {
            $rules += [
                'banner_image'      => 
                [
                    'required',
                    'mimes:jpeg,png,jpg,gif,webp', 
                    'max:2048'
                ],
                'slug'                  => 'required|unique:blogs,slug',
            ];
        }
        elseif ($this->getMethod() == 'PUT') {
            $rules += [
                'banner_image'        =>  
                [
                    'mimes:jpeg,png,jpg,gif,webp',
                    'max:2048'
                ],
                'slug'                  => 'required|unique:blogs,slug,'.$this->route('blog'),
            ];
        }
        return $rules;
    }
    public function messages()
    {
        return [
            'min_of_read.required'              => 'The minutes of read field is required.',
            'min_of_read.numeric'               => 'Enter number  only.',
            'banner_image.max'                  => 'The maximum file size limit is 2 mb.'
        ];
    }
}
