<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SchoolFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            // 'name'              => 'required',
            'address'           => 'required',
            'official_contact_no' => 'required',
        ];
        if ($this->getMethod() == 'POST') {
            $rules += [
                'name'                  => 'required|unique:schools,name',
            ];
        }
        elseif ($this->getMethod() == 'PUT') {
            $rules += [
                'name'                  => 'required|unique:schools,name,'.$this->route('school'),
            ];
        }
        return $rules;
    }
}
