<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LessonFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'school_id' => 'required',
            'grade_id' => 'required',
            'term_id' => 'required',  
            'title' => 'required',  
            'lesson_name' => 'required', 
            'how_do_they_learn' => 'required', 
            'what_are_the_learing_outcome' => 'required',  
            'assignment' => 'required',
            // 'type' => 'required',
            // 'description' => 'required',
        ];         
        if ($this->getMethod() == 'POST') {
            $rules += [
                'position'                  => 'required|unique:lessons,position',
            ];
        }
        elseif ($this->getMethod() == 'PUT') {
            $rules += [
                'position'                  => 'required|unique:lessons,position,'.$this->route('lesson'),
            ];
        }
                                             
        return $rules;
    }
    public function messages()
    {
        return [
            'title.required'              => 'The what do they learn field is required.',
        ];
    }


}
