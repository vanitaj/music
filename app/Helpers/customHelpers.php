<?php
//use App\Services\DOMediaService;
//use Illuminate\Support\Facades\Url;
//use Illuminate\Http\File;
ini_set('memory_limit', -1);
ini_set('max_execution_time', 0);

// if (!function_exists('file_driver')) {
//     function file_driver()
//     {               
//         return 'spaces';
//     }
// }

if (!function_exists('base_url')) {
    function base_url()
    {               
        return URL::to('/');
    }
}

if (!function_exists('media_upload')) {
	function media_upload($file, $path, $file_prefix = null)
	{
       
       //check_folder_exists($path);
	    // $name = media_unique_name($file_prefix) . '.' . $file->getClientOriginalExtension();
        $name =  $file->getClientOriginalName();
	    $file->move($path, $name);
	    return $path . '/' . $name;
	}
}

if (!function_exists('media_upload_compress')) {
    function media_upload_compress($file, $path, $file_prefix = null)
    {    
       
        set_time_limit(0);
        $extension = $file->getClientOriginalExtension();
        
        $list = ['JPEG','JPG','PNG','GIF','BMP','WEBP'];
        if(in_array(strtoupper($extension), $list))
        {
            check_folder_exists($path);
            // $name = media_unique_name($file_prefix) . '.' . $extension;        
            $name =  $file->getClientOriginalName();
            // dd($name);
            $fileNameWithoutExt = pathinfo($name, PATHINFO_FILENAME);
            $completePath = $path . '/' . $fileNameWithoutExt.'.webp';
            Image::make($file)->encode('webp',90)->save($completePath);       
            return $completePath;
        }
        
        return media_upload($file, $path, $file_prefix);
    }
}

// if (!function_exists('media_upload_to_cloud')) {
//     function media_upload_to_cloud($file, $path)
//     {     
//         $fileNameWithoutExt = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
//         $fileExtension = $file->getClientOriginalExtension();


//         //$doFilePath = $path . '/' . $fileNameWithoutExt . '-'. time() .'.' . $fileExtension; //Digital ocean file path
//          $doFilePath = $path;  //Digital ocean file path
//          $doFileName = $fileNameWithoutExt . '-'. time() .'.' . $fileExtension;
        
//          \Storage::disk(file_driver())->putFileAs($doFilePath, $file, $doFileName); 
//         //\Storage::disk(file_driver())->put($doFilePath, $file);       
//         return $doFilePath.'/'.$doFileName;
//     }
// }


// if (!function_exists('media_unique_name')) {
// 	function media_unique_name($file_prefix)
// 	{
// 	    if (!isset($file_prefix)) {
// 	        $file_prefix = env('APP_NAME');
// 	        $file_prefix .= '-';
// 	    }
// 	    $unique_string = uniqid($file_prefix, true);
// 	    $str = "1234567890abcdefghijklmnopqrstuvwxyz()$";
// 	    $rand_string = substr(str_shuffle($str), 0, 8);
// 	    $unique_string .= $rand_string;
// 	    return $unique_string;
// 	}
// }

if (!function_exists('media_file')) {
    function media_file($value)
    {
        if (isset($value)) {
        
            $extension = get_file_extension($value);            
            
            switch ($extension) {
             case 'pdf':
               $file ='admin/images/pdf.png';
               break;

             default:
               $file = $value;
               break;
           }
           return asset($file);
        }else{
            return asset('admin/images/no-image.jpg');
        } 
        
    }
}

if (!function_exists('resource_file')) {
    function resource_file($value)
    {
        if (isset($value)) {
        
            $extension = get_file_extension($value);            
            
            switch ($extension) {
            case 'pdf':
               $file ='admin/images/pdf.png';
               break;
            case 'mp3':
                $file ='admin/images/mp3-icon.png';
                break;
            default:
                $file ='admin/images/doc-icon.jpg';
               break;
           }
           return asset($file);
        }else{
            return asset('admin/images/no-image.jpg');
        } 
        
    }
}


if (!function_exists('get_file_extension')) {
    function get_file_extension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);        
    }
}

if (!function_exists('media_delete')) {
    function media_delete($data)
    {
        if(File::exists(public_path($data))){
            File::delete(public_path($data));
        }
        return;
    }
}

if (!function_exists('check_folder_exists')) {
    function check_folder_exists($path)
    {    	
    	$path = public_path($path);

	    if(!File::isDirectory($path)){
	        File::makeDirectory($path, 0777, true, true);
	    }
	    return;    	
    }
}

if (!function_exists('print_value')) {
    function print_value($old_value, $db_value)
    {
    	return $old_value ? $old_value : $db_value;
    }
}

if (!function_exists('str_val')) {
    function str_val($value)
    {
        return strval($value);
        //return (string) $value;
    }
}

if (!function_exists('short_text')) {
    function short_text($content, $no_of_chars)
    {    	
    	
       return strlen($content) < $no_of_chars ? $content : substr($content, 0, stripos($content, ' ', $no_of_chars) ).' ...';     	
    }
}

if (!function_exists('formatted_date')) {
    function formatted_date($date)
    {     

        return  $date == NULL ? '': Carbon\Carbon::parse($date)->format('d M Y');
        
    }
}

if (!function_exists('current_date')) {
    function current_date()
    {  
        return  Carbon\Carbon::now();
    }
}

// if (!function_exists('video_url')) {
//     function video_url($url)
//     {     
//         parse_str( parse_url( $url, PHP_URL_QUERY ), $array_of_vars );
//         $youtubeId = isset($array_of_vars['v'])? $array_of_vars['v']: "";
//         return "https://www.youtube.com/embed/".$youtubeId;        
//     }
// }

// if (!function_exists('video_id')) {
//     function video_id($url)
//     {     
//         return preg_replace('/[^0-9]/','',$url);        
//     }
// }

// if (!function_exists('vimeo_url')) {
//     function vimeo_url($id)
//     {     
//         return 'https://vimeo.com/'.$id;     
//     }
// }

// if (!function_exists('view_url')) {
//     function view_url($name)
//     {  
//         return url('https://gmsstagecdn.ams3.cdn.digitaloceanspaces.com').'/'.$name;        
//     }
// }

if (!function_exists('slug_format')) {
    function slug_format($slug)
    {  
        return strtolower(str_replace(' ', '-', $slug));        
    }
}

// if (!function_exists('tide_type')) {
//     function tide_type($type)
//     {  
//         return config('constants.tide_type.'.$type);             
//     }
// }

// if (!function_exists('current_year')) {
//     function current_year()
//     {               
//         return \Carbon\Carbon::now()->year;
//     }
// }

if (!function_exists('media_file_backend')) {
    function media_file_backend($value)
    {  
        return DOMediaService::mediaFileBackend($value);        
    }
}

if (!function_exists('media_file_frontend')) {
    function media_file_frontend($value)
    {   
        return DOMediaService::mediaFileFrontend($value);
    }
}

// if (!function_exists('vimeo_player')) {
//     function vimeo_player($value)
//     {  
//         return "https://player.vimeo.com/video/$value";
//     }
// }

// if (!function_exists('registerUserOtpExpiryInSeconds')) {
//     function registerUserOtpExpiryInSeconds()
//     {       
//         return \Carbon\Carbon::now()->addSeconds(config( 'constants.registerUserOtpValidForSeconds' ));
//     }
// }

// if (!function_exists('forgotPasswordOtpExpiryInSeconds')) {
//     function forgotPasswordOtpExpiryInSeconds()
//     {       
//         return \Carbon\Carbon::now()->addSeconds(config( 'constants.forgotPasswordOtpExpiryInSeconds' ));
//     }
// }

// if (!function_exists('getOtpStr')) {
//     function getOtpStr()
//     {
//         return rand ( 1000 , 9999 );
//     }
// }