<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Throwable;
use Exception;
//use App\Traits\ApiResponser;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Auth;

class Handler extends ExceptionHandler
{
    //use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];    

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        $guard = Arr::get($exception->guards(), 0);

       switch ($guard) {
         case 'admin':
           $login='admin.login';
           break;

         default:
           $login='login';
           break;
       }       
        return redirect()->guest(route($login));
    }

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(
            function (Exception $exp, $request) 
            { 
            if( $request->wantsJson() || $request->is('api/*') )
              {
                //return $this->handleApiJsonException($exp, $request);
              }//end of api check
                
            }
        );
    }

    public function handleApiJsonException(Exception $exp, $request)
    {
        //dd($exp);
        if ($exp instanceof AuthenticationException) 
        {
            return $this->errorJsonResponse($exp->getMessage(), 401);
        }

        if ($exp instanceof MethodNotAllowedHttpException) 
        {
            return $this->errorJsonResponse('The specified method for the request is invalid', 405, 404);
        }

        if ($exp instanceof NotFoundHttpException) {
            return $this->errorJsonResponse('The specified URL cannot be found', 404, 404);
        }        

        if ($exp instanceof ModelNotFoundException ) {
            return $this->errorJsonResponse($exp->getMessage(), $exp->getStatusCode());
        }

        if ($exp instanceof \Illuminate\Database\QueryException ) {
            return $this->errorJsonResponse('Unexpected Database Exception. Try later', 500, 500);
        }

        if ($exp instanceof ThrottleRequestsException) {
          return $this->errorJsonResponse('Too Many Attempts. Please try after '.$exp->getHeaders()['Retry-After'] .' seconds', 429);
        }

        if ($exp instanceof HttpException) {
            return $this->errorResponse($exp->getMessage(), $exp->getStatusCode());
        }

        if ($exp instanceof \ErrorException ) {
            return $this->errorJsonResponse($exp->getMessage(), 500, 500);
        }

        if ($exp instanceof \Error) {
            return $this->errorJsonResponse('Internal server error.', 500, 500);
        }

        if (config('app.debug')) {
            return parent::render($request, $exp);            
        }

        return $this->errorJsonResponse('Unexpected Exception. Try later', 500, 500);
    }
}
