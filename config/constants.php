<?php
// if(config('app.env') == 'production')
// {
//     $ipaPassword = env('IPA_LIVE_PASSWORD');
//     $ipaVerifyReceiptUrl = env('IPA_LIVE_URL', 'https://sandbox.itunes.apple.com/verifyReceipt');
// }
// else 
// {
//     $ipaPassword = env('IPA_SAND_PASSWORD');
//     $ipaVerifyReceiptUrl = 'https://sandbox.itunes.apple.com/verifyReceipt';
// }

return [


    /*
    |---------------------------------------------------
    | Images Constants
    |---------------------------------------------------
    */

    'uploads' => [
        'profile_images'    => 'uploads/profile_image',
        'user_file'         => 'uploads/user_files',
    	'encrypted_files'   => 'encrypted_files/',
    	'decrypted_files'   => 'decrypted_files/',
        'home_video_image'  => 'uploads/home_video_image',
        'testimonial'       => 'uploads/testimonial',
        'fact'              => 'uploads/fact',
        'banner'            => 'uploads/banner',
        'resource'          => 'uploads/resource',
        'assignment'        => 'uploads/assignment',
    ],

    // 'uploads' => [
    //     'profile_image'     => config('app.do_spaces_folder').'/profile_image',
    //     'user_file'         => config('app.do_spaces_folder').'/user_files',
    //     'encrypted_files'   => 'encrypted_files/',
    //     'decrypted_files'   => 'decrypted_files/',
    // ],

    /*
    |--------------------------------------------------------------
    | Email Constants
    |--------------------------------------------------------------
    */
 
    'mail' => [
        'writeToUsMailAdmin'    => env('WRITETOUS_MAIL_TO_ADMIN', 'dev.mutefrog@gmail.com'),
        'noReply'               => env('NO_REPLY_MAIL', 'dev.mutefrog@gmail.com'),
        'mailTo'        => env('USERAUTH_MAIL_TO_ADMIN', 'dev.mutefrog@gmail.com'),
        'mailFrom'      => env('USERAUTH_MAIL_FROM_ADMIN', 'dev.mutefrog@gmail.com'),
    ],

      /*
    |--------------------------------------------------------------------------
    | Backend Crud Messages
    |--------------------------------------------------------------------------
    */

    'message'       => [
        'save'      => 'Record saved successfully',
        'update'    => 'Record updated successfully',
        'delete'    => 'Record deleted successfully',
        'no_record' => 'Record not found',
    ],

];